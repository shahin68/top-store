package com.apptech.topstore.utils;

import android.content.Context;

import com.apptech.topstore.AppController;

public class StatusBarHeight {
    public int get() {
        int result = 0;
        int resourceId = AppController.getContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = AppController.getContext().getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
