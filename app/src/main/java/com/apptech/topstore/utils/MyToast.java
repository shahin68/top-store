package com.apptech.topstore.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.apptech.topstore.AppController;
import com.apptech.topstore.R;


public class MyToast {

    @Nullable
    private static Toast theToast;

    public static Toast warning(String message){
        if (theToast != null){
            theToast.cancel();
        }
        theToast = Toast.makeText(AppController.getContext(), message, Toast.LENGTH_LONG);
        LayoutInflater tInflater = (LayoutInflater) AppController.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert tInflater != null;
        View view = tInflater.inflate(R.layout.toast, null);
        theToast.setView(view);
        TextView toastText = (TextView)view.findViewById(R.id.toast_txt);
        CardView toastCard = (CardView) view.findViewById(R.id.toast_card);


        toastCard.setCardBackgroundColor(ContextCompat.getColor(AppController.getContext(), R.color.amber500));
        toastText.setText(message);
        toastText.setTypeface(Fonts.IranNormal(AppController.getContext()));

        float lightness = Lightness(ContextCompat.getColor(AppController.getContext(), R.color.amber500));
        if (lightness >= 0.5){
            toastText.setTextColor(ContextCompat.getColor(AppController.getContext(), R.color.black));
        } else if (lightness < 0.5){
            toastText.setTextColor(ContextCompat.getColor(AppController.getContext(), R.color.white));
        }

        return theToast;
    }
    public static Toast error(String message){
        if (theToast != null){
            theToast.cancel();
        }
        theToast = Toast.makeText(AppController.getContext(), message, Toast.LENGTH_LONG);
        LayoutInflater tInflater = (LayoutInflater) AppController.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert tInflater != null;
        View view = tInflater.inflate(R.layout.toast, null);
        theToast.setView(view);
        TextView toastText = (TextView)view.findViewById(R.id.toast_txt);
        CardView toastCard = (CardView) view.findViewById(R.id.toast_card);


        toastCard.setCardBackgroundColor(ContextCompat.getColor(AppController.getContext(), R.color.red300));
        toastText.setText(message);
        toastText.setTypeface(Fonts.IranNormal(AppController.getContext()));

        float lightness = Lightness(ContextCompat.getColor(AppController.getContext(), R.color.red300));
        if (lightness >= 0.5){
            toastText.setTextColor(ContextCompat.getColor(AppController.getContext(), R.color.black));
        } else if (lightness < 0.5){
            toastText.setTextColor(ContextCompat.getColor(AppController.getContext(), R.color.white));
        }



        return theToast;
    }
    public static Toast good(String message){
        if (theToast != null){
            theToast.cancel();
        }
        theToast = Toast.makeText(AppController.getContext(), message, Toast.LENGTH_LONG);
        LayoutInflater tInflater = (LayoutInflater) AppController.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert tInflater != null;
        View view = tInflater.inflate(R.layout.toast, null);
        theToast.setView(view);
        TextView toastText = (TextView)view.findViewById(R.id.toast_txt);
        CardView toastCard = (CardView) view.findViewById(R.id.toast_card);


        toastCard.setCardBackgroundColor(ContextCompat.getColor(AppController.getContext(), R.color.tealA400));
        toastText.setText(message);
        toastText.setTypeface(Fonts.IranNormal(AppController.getContext()));

        float lightness = Lightness(ContextCompat.getColor(AppController.getContext(), R.color.tealA400));
        if (lightness >= 0.5){
            toastText.setTextColor(ContextCompat.getColor(AppController.getContext(), R.color.black));
        } else if (lightness < 0.5){
            toastText.setTextColor(ContextCompat.getColor(AppController.getContext(), R.color.white));
        }



        return theToast;
    }
    private static float Lightness(int color){
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);

        float hsl[] = new float[3];
        ColorUtils.RGBToHSL(red, green, blue, hsl);
        return hsl[2];
    }
}
