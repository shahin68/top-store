/*
 * Created & Programmed by shahin montazeri (shahin.montazeri@gmail.com)
 * Copyright (c) 2018 at mediahamrah. All rights reserved.
 */

package com.apptech.topstore.utils;

import java.text.DecimalFormat;

public class ConvertNumbers {
    public static String convertToPersian(String str, boolean priceType) {
        String[][] chars = new String[][]{
                {"0", "۰"},
                {"1", "۱"},
                {"2", "۲"},
                {"3", "۳"},
                {"4", "۴"},
                {"5", "۵"},
                {"6", "۶"},
                {"7", "۷"},
                {"8", "۸"},
                {"9", "۹"}
        };

        if (priceType){
            DecimalFormat formatter = new DecimalFormat("###,###,###");
            str = formatter.format(Integer.valueOf(str));
            for (String[] num : chars) {
                str = str.replace(num[0], num[1]);
            }
        } else {
            for (String[] num : chars) {
                str = str.replace(num[0], num[1]);
            }
        }

        return str;
    }

    public static String convertToLatin(String str) {
        String[][] chars = new String[][]{
                {"0", "۰"},
                {"1", "۱"},
                {"2", "۲"},
                {"3", "۳"},
                {"4", "۴"},
                {"5", "۵"},
                {"6", "۶"},
                {"7", "۷"},
                {"8", "۸"},
                {"9", "۹"}
        };

        for (String[] num : chars) {
            str = str.replace(num[1], num[0]);
        }

        return str;
    }

}
