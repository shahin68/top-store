package com.apptech.topstore.utils.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

public class MySharedPreferences {
    private static SharedPreferences preferences;

    public static void setPreferences(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static SharedPreferences getPreferences() {
        return preferences;
    }

    public static void saveString(String tag, String string){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putString(tag , string);
        editor.apply();
    }
    public static String getString(String tag, String defVal){
        return getPreferences().getString(tag, defVal);
    }

    public static void saveStringSet(String tag, Set<String> string){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putStringSet(tag , string);
        editor.apply();
    }
    public static Set<String> getStrinSet(String tag, Set<String> defVal){
        return getPreferences().getStringSet(tag, defVal);
    }


    public static void saveLong(String tag, long val){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(tag , val);
        editor.apply();
    }
    public static long getLong(String tag, long defVal){
        return getPreferences().getLong(tag, defVal);
    }

    public static void saveAlarmStatus(String tag, boolean val){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putBoolean(tag , val);
        editor.apply();
    }

    public static boolean getAlarmStatus(String tag, boolean defVal){
        return getPreferences().getBoolean(tag, defVal);
    }

    public static void saveLongPeriod(String tag, long val){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putLong(tag , val);
        editor.apply();
    }

    public static long getLongPeriod(String tag, long defVal){
        return getPreferences().getLong(tag, defVal);
    }

    public static void savePreviousRand(String tag, int val){
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(tag , val);
        editor.apply();
    }

    public static int getPreviousRand(String tag, int defVal){
        return getPreferences().getInt(tag, defVal);
    }


    public static void saveInteger(String tag, int val){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(tag , val);
        editor.apply();
    }
    public static int getInteger(String tag, int defVal){
        return getPreferences().getInt(tag, defVal);
    }


    public static void saveBoolean(String tag, boolean val){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(tag , val);
        editor.apply();
    }
    public static boolean getBoolean(String tag, boolean defVal){
        return getPreferences().getBoolean(tag, defVal);
    }
}
