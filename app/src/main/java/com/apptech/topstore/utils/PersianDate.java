package com.apptech.topstore.utils;

import com.aminography.primecalendar.PrimeCalendar;
import com.aminography.primecalendar.common.CalendarFactory;
import com.aminography.primecalendar.common.CalendarType;

import java.util.Calendar;

public class PersianDate {
    public static String weekDay(long time){
        PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
        primeCalendar.setTimeInMillis(time);
        return primeCalendar.getWeekDayName();
    }
    public static String date(long time){
        PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
        primeCalendar.setTimeInMillis(time);
        return primeCalendar.getWeekDayName() + " - " +primeCalendar.getDayOfMonth() + " " + primeCalendar.getMonthName() + " " + primeCalendar.getYear() + " - " + primeCalendar.get(Calendar.HOUR_OF_DAY) + ":" + primeCalendar.get(Calendar.MINUTE);
    }
}
