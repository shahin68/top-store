package com.apptech.topstore.utils;

import android.icu.util.Calendar;

import com.aminography.primecalendar.PrimeCalendar;
import com.aminography.primecalendar.common.CalendarFactory;
import com.aminography.primecalendar.common.CalendarType;

public class Today {
    public String weekDay(){
        PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
        primeCalendar.setTimeInMillis(System.currentTimeMillis());
        return primeCalendar.getWeekDayName();
    }
    public String date(){
        PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
        primeCalendar.setTimeInMillis(System.currentTimeMillis());
        return primeCalendar.getDayOfMonth() + " " + primeCalendar.getMonthName() + " " + primeCalendar.getYear();
    }
}
