/*
 * Created by shahin montazeri (shahin.montazeri@gmail.com)
 * Copyright (c) 2018 at mediahamrah. All rights reserved.
 */

package com.apptech.topstore.utils;

import android.content.Context;
import android.graphics.Typeface;

public class Fonts {

    // ---------------- Iran Sans Fonts ------------------------------------------------------------

    public static Typeface IranNormal(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts/iran_sans.ttf");
    }

    public static Typeface IranLight(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts/iran_sans_light.ttf");
    }

    public static Typeface IranBold(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts/iran_sans_bold.ttf");
    }

}
