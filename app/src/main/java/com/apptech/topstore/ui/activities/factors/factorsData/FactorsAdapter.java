package com.apptech.topstore.ui.activities.factors.factorsData;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.ui.activities.factorDetails.FactorDetailsActivity;
import com.apptech.topstore.ui.activities.factors.FactorsActivity;
import com.apptech.topstore.ui.activities.factors.FactorsViewModel;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.PersianDate;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FactorsAdapter extends RecyclerView.Adapter<FactorsAdapter.myViewHolder>{
    List<FactorModel> items = new ArrayList<>();
    private Context mContext;
    private LayoutInflater inflater;
    private FactorsViewModel mViewModel;

    String mSearchPhrase;

    public FactorsAdapter(Context context, List<FactorModel> arrayList, String searchPhrase, FactorsViewModel factorsViewModel){
        this.mContext = context;
        this.items = arrayList;
        inflater = LayoutInflater.from(context);
        this.mSearchPhrase = searchPhrase;
        mViewModel = factorsViewModel;

        Collections.sort(items, (o1, o2) -> {
            if ((o1.getFactorReferenceCode()+"-"+o1.getFactorProductCount()+"-"+o1.getFactorCost()+"-"+PersianDate.date(o1.getFactorDateCreated())).contains(latinNumOf(searchPhrase)) && !(o2.getFactorReferenceCode()+"-"+o2.getFactorProductCount()+"-"+o2.getFactorCost()+"-"+PersianDate.date(o2.getFactorDateCreated())).contains(latinNumOf(searchPhrase))){
                return -1;
            } else if (!(o1.getFactorReferenceCode()+"-"+o1.getFactorProductCount()+"-"+o1.getFactorCost()+"-"+PersianDate.date(o1.getFactorDateCreated())).contains(latinNumOf(searchPhrase)) && (o2.getFactorReferenceCode()+"-"+o2.getFactorProductCount()+"-"+o2.getFactorCost()+"-"+PersianDate.date(o2.getFactorDateCreated())).contains(latinNumOf(searchPhrase))){
                return 1;
            } else if ((o1.getFactorReferenceCode()+"-"+o1.getFactorProductCount()+"-"+o1.getFactorCost()+"-"+PersianDate.date(o1.getFactorDateCreated())).contains(latinNumOf(searchPhrase)) && (o2.getFactorReferenceCode()+"-"+o2.getFactorProductCount()+"-"+o2.getFactorCost()+"-"+PersianDate.date(o2.getFactorDateCreated())).contains(latinNumOf(searchPhrase))){
                return 0;
            } else {
                return (o1.getFactorReferenceCode()+"-"+o1.getFactorProductCount()+"-"+o1.getFactorCost()+"-"+PersianDate.date(o1.getFactorDateCreated())).compareToIgnoreCase(o2.getFactorReferenceCode()+"-"+o2.getFactorProductCount()+"-"+o2.getFactorCost()+"-"+PersianDate.date(o2.getFactorDateCreated()));
            }
        });
    }
    private String latinNumOf(String num) {
        return ConvertNumbers.convertToLatin(num);
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView = inflater.inflate(R.layout.item_factors, parent, false);
        return new myViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {


        /*mViewModel.getSoldProducts(items.get(position).getFactorID()).observe(FactorsActivity.getInstance(), new Observer<List<SoldProductModel>>() {
            @Override
            public void onChanged(List<SoldProductModel> soldProductModels) {
                // TODO NOW USE this model to show your data
            }
        });*/



        if (!mSearchPhrase.isEmpty()){
            holder.mFactorNumber.setText(Html.fromHtml(mContext.getString(R.string.factor) + " " + ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getFactorReferenceCode()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>"+latinNumOf(mSearchPhrase)+"</font>"),false)));
            holder.mProductsCount.setText(Html.fromHtml(ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getFactorProductCount()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>"+latinNumOf(mSearchPhrase)+"</font>"), false) + " " + mContext.getString(R.string.num)));
            holder.mDate.setText(Html.fromHtml(ConvertNumbers.convertToPersian(PersianDate.date(items.get(position).getFactorDateCreated()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>" + latinNumOf(mSearchPhrase) + "</font>"), false)));
            holder.mPrice.setText(Html.fromHtml("<html><body>" + String.valueOf(items.get(position).getFactorCost()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>"+latinNumOf(mSearchPhrase)+"</font>") + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));

            if (!(items.get(position).getFactorReferenceCode()+"-"+items.get(position).getFactorProductCount()+"-"+PersianDate.date(items.get(position).getFactorDateCreated())+"-"+items.get(position).getFactorCost()).contains(latinNumOf(mSearchPhrase))){
                holder.itemView.setAlpha(.3f);
            } else {
                holder.itemView.setAlpha(1);
            }
        } else {
            holder.mFactorNumber.setText(mContext.getString(R.string.factor) + " " + ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getFactorReferenceCode()),false));
            holder.mProductsCount.setText(ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getFactorProductCount()), false) + " " + mContext.getString(R.string.num));
            holder.mDate.setText(ConvertNumbers.convertToPersian(PersianDate.date(items.get(position).getFactorDateCreated()), false));
            holder.mPrice.setText(Html.fromHtml("<html><body>" + ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getFactorCost()), true) + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));
            holder.itemView.setAlpha(1);
        }


        if (items.get(position).getFactorType() == 0){ // returned - rejected
            holder.mStatusImage.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_return));
            holder.mStatusImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.background_orange));
        } else if (items.get(position).getFactorType() == 1){ // positive - bought
            holder.mStatusImage.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_arrow_negative));
            holder.mStatusImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.background_negative));
        } else if (items.get(position).getFactorType() == 2){ // negative - sold
            holder.mStatusImage.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_arrow_positive));
            holder.mStatusImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.background_positive));
        } else { // unknown
            holder.mStatusImage.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_return));
            holder.mStatusImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.background_orange));
        }




    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public Context getContext() {
        return mContext;
    }

    class myViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.itemFactorsNumber) TextView mFactorNumber;
        @BindView(R.id.itemFactorsCount) TextView mProductsCount;
        @BindView(R.id.itemFactorsDate) TextView mDate;
        @BindView(R.id.itemFactorsPrice) TextView mPrice;
        @BindView(R.id.itemFactorsStatusImg) AppCompatImageView mStatusImage;


        public myViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mFactorNumber.setTypeface(Fonts.IranBold(mContext));
            mProductsCount.setTypeface(Fonts.IranNormal(mContext));
            mDate.setTypeface(Fonts.IranLight(mContext));
            mPrice.setTypeface(Fonts.IranBold(mContext));


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                itemView.setForeground(ContextCompat.getDrawable(mContext,R.drawable.ripple_accent_general));
            }




            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, FactorDetailsActivity.class);
            intent.putExtra("factor_ID", items.get(getAdapterPosition()).getFactorID());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            v.getContext().startActivity(intent);
        }
    }

    // TODO FIX THIS LATER
    public void deleteItem(int position) {
//        showUndoSnackBar(position, items.get(position));
        mViewModel.delete(items.get(position));
        mViewModel.deleteFactorSoldProducts(FactorsActivity.getInstance(), items.get(position));
        items.remove(position);
        notifyItemRemoved(position);
    }
    private void showUndoSnackBar(int position, FactorModel factorModel) {
        View view = FactorsActivity.getInstance().findViewById(R.id.factors_layout);
        Snackbar snackbar = Snackbar.make(view, mContext.getString(R.string.product_removed), Snackbar.LENGTH_LONG);
        snackbar.setAction(mContext.getString(R.string.undo_remove), v -> undoDelete(position, factorModel));
        snackbar.show();
        snackbar.addCallback(new Snackbar.Callback(){
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                mViewModel.delete(factorModel);
            }
        });
    }
    private void undoDelete(int position, FactorModel factorModel) {
        items.add(position, factorModel);
        notifyItemInserted(position);
    }
}
