package com.apptech.topstore.ui.activities.initialSettings.contents;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.databaseLocal.Currency;
import com.apptech.topstore.databaseLocal.Languages;
import com.apptech.topstore.ui.activities.home.HomeActivity;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsInitialContent {
    // --- toolbar views
    @BindView(R.id.settingsInitialTitle) TextView mTitle;

    // --- Contents
    @BindView(R.id.settingsInitialCurrency) CardView mCurrency;
    @BindView(R.id.settingsInitialCurrencyTxt) TextView mCurrencyTxt;
    @BindView(R.id.settingsInitialLanguage) CardView mLanguage;
    @BindView(R.id.settingsInitialLanguageTxt) TextView mLanguageTxt;
    @BindView(R.id.settingsInitialStoreName) TextInputEditText mStoreNameInput;
    @BindView(R.id.settingsInitialUserName) TextInputEditText mUserNameInput;
    @BindView(R.id.settingsInitialStoreBtn) Button mStoreSettings;


    public SettingsInitialContent(Activity activity){
        ButterKnife.bind(this, activity);
        mTitle.setTypeface(Fonts.IranBold(activity));
        mStoreSettings.setTypeface(Fonts.IranBold(activity));
        mCurrencyTxt.setTypeface(Fonts.IranBold(activity));
        mLanguageTxt.setTypeface(Fonts.IranBold(activity));
        mStoreNameInput.setTypeface(Fonts.IranBold(activity));
        mUserNameInput.setTypeface(Fonts.IranBold(activity));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mStoreSettings.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mCurrency.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mLanguage.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }


        mCurrency.setOnClickListener(v -> {
            showCurrencyMenu(activity, v);
        });
        mLanguage.setOnClickListener(v -> {
            showLanguageMenu(activity, v);
        });
        mStoreSettings.setOnClickListener(v -> {
            openHomeActivity(activity);
        });
    }

    public void openHomeActivity(Activity activity){
        MyLog.LogDebugging("LoginActivity() ---> " + "openHomeActivity()");
        Intent intent = new Intent(activity, HomeActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        activity.finish();
    }

    private void showCurrencyMenu(Activity activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Currency.countryCurrencies().length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    Currency.countryCurrencies()[i].first);
        }
        popup.setOnMenuItemClickListener(
                item -> {
                    mCurrencyTxt.setText(item.getTitle());
                    return true;
                });
        popup.show();
    }
    private void showLanguageMenu(Activity activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Languages.get().length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    Languages.get()[i]);
        }
        popup.setOnMenuItemClickListener(
                item -> {
                    mLanguageTxt.setText(item.getTitle());
                    return true;
                });
        popup.show();
    }
}
