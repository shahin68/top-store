package com.apptech.topstore.ui.activities.home.navDrawer;

import java.util.ArrayList;

public class DrawerItem {
    public static ArrayList<DrawerItem> folderItems = new ArrayList<>();
    public String nav_title; // nav item Title
    public int nav_img; // image Resource
}
