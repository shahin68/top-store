package com.apptech.topstore.ui.activities.factors.factorsData;

public class FactorsFakeData {
    public static Integer Count(){
        return 21;
    }
    public static int numbers(int pick){
        int[] ints = new int[]{
                1844,
                485,
                40,
                30,
                857,
                245,
                125,
                288,
                65457,
                99,
                454,
                75,
                624,
                288,
                46234,
                245,
                125,
                288,
                30,
                857,
                245
        };

        return ints[pick];
    }
    public static int price(int pick){
        int[] ints = new int[]{
                184400,
                485000,
                400000,
                30000,
                857000,
                245000,
                125900,
                28800,
                65000,
                9900,
                454000,
                485000,
                400000,
                30000,
                857000,
                245000,
                125900,
                28800,
                65000,
                485000,
                400000
        };

        return ints[pick];
    }
    public static int status(int pick){
        int[] ints = new int[]{
                0,
                2,
                2,
                1,
                0,
                2,
                1,
                0,
                2,
                1,
                1,
                2,
                2,
                1,
                0,
                2,
                1,
                0,
                2,
                1,
                0
        };

        return ints[pick];
    }
    public static int counts(int pick){
        int[] ints = new int[]{
                33,
                35,
                72,
                17,
                15,
                844,
                345,
                564,
                567,
                99584,
                25,
                33,
                35,
                72,
                573,
                15,
                844,
                345,
                564,
                567,
                18
        };

        return ints[pick];
    }
    public static String dates(int pick){
        String[] ints = new String[]{
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30",
                "23 آبان 98 - 5:30"
        };

        return ints[pick];
    }
}
