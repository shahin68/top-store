package com.apptech.topstore.ui.activities.factorDetails.contents;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.database.factors.soldProducts.SoldProductModel;
import com.apptech.topstore.ui.activities.factorDetails.FactorDetailsActivity;
import com.apptech.topstore.ui.activities.factorDetails.FactorDetailsData.FactorDetailsAdapter;
import com.apptech.topstore.ui.activities.factorDetails.FactorDetailsData.FactorDetailsAdapterSwipeToDeleteCallback;
import com.apptech.topstore.ui.activities.factorDetails.FactorDetailsViewModel;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FactorDetailsContent {
    FactorDetailsViewModel mViewModel;

    // --- toolbar views
    @BindView(R.id.factorDetailsTitle) TextView mTitle;
    @BindView(R.id.factorDetailsBack) LinearLayout mBack;
    @BindView(R.id.factorDetailsTopDecor) AppCompatImageView mTopDecor;

    // --- list
    @BindView(R.id.factorDetails_list) RecyclerView mList;



    public FactorDetailsContent(Activity activity, FactorDetailsViewModel factorsViewModel, List<SoldProductModel> soldProductModels, int factorID){
        mViewModel = factorsViewModel;
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.IranBold(activity));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
        }

        mViewModel.getFactorByID(factorID).observe(FactorDetailsActivity.getInstance(), factorModel -> {
            setTitle(activity.getString(R.string.factor) + " " + ConvertNumbers.convertToPersian(String.valueOf(factorModel.getFactorReferenceCode()),false));
            initList(activity, soldProductModels, factorModel);
        });

    }
    public void initList(Context context, List<SoldProductModel> soldProductModels, FactorModel factorModel){
        MyLog.LogDebugging("FactorDetailsActivity() ---> " + "initList() ---> " + " | FINAL LIST SIZE = " + soldProductModels.size());
        FactorDetailsAdapter adapter = new FactorDetailsAdapter(context, soldProductModels, factorModel, mViewModel);
        mList.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new FactorDetailsAdapterSwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(mList);
        adapter.notifyDataSetChanged();
        mList.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
    }

    public void setTitle(String factorName) {
        mTitle.setText(ConvertNumbers.convertToPersian(factorName, false));
    }

}
