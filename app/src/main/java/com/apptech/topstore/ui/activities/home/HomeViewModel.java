package com.apptech.topstore.ui.activities.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.apptech.topstore.database.AppDatabase;
import com.apptech.topstore.database.factors.FactorModel;

import java.util.List;

import javax.inject.Inject;

public class HomeViewModel extends ViewModel {

    @Inject
    AppDatabase appDatabase;

    public LiveData<List<FactorModel>> getFactors() {
        return appDatabase.factorsDao().getAllFactorsLive();
    }
}
