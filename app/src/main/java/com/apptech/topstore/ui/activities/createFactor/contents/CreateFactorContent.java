package com.apptech.topstore.ui.activities.createFactor.contents;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.GenerateFactorID;
import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.database.factors.soldProducts.SoldProductModel;
import com.apptech.topstore.ui.activities.createFactor.CreateFactorActivity;
import com.apptech.topstore.ui.activities.createFactor.CreateFactorViewModel;
import com.apptech.topstore.ui.activities.createFactor.SoldProducts;
import com.apptech.topstore.ui.activities.createFactor.productsData.FactorsSoldAdapterSwipeToDeleteCallback;
import com.apptech.topstore.ui.activities.createFactor.productsData.FactorsSoldProductsAdapter;
import com.apptech.topstore.ui.activities.customerName.CustomerName;
import com.apptech.topstore.ui.activities.newProduct.NewProductActivity;
import com.apptech.topstore.ui.activities.storage.StorageActivity;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;
import com.apptech.topstore.utils.preferences.MyPreferences;
import com.apptech.topstore.utils.preferences.MySharedPreferences;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateFactorContent {
    CreateFactorViewModel mViewModel;
    List<SoldProductModel> mSoldProductsModel;
    // --- toolbar views
    @BindView(R.id.createFactorTitle) TextView mTitle;
    @BindView(R.id.createFactorBack) LinearLayout mBack;
    @BindView(R.id.createFactorTopDecor) AppCompatImageView mTopDecor;
    @BindView(R.id.createFactorBtn) Button mStoreFactor;

    // ---- contents
    @BindView(R.id.factorNewProduct) CardView mNewProduct;
    @BindView(R.id.factorAddProduct) CardView mAddProduct;
    @BindView(R.id.factorBarcodeReader) CardView mBarcodeReader;
    @BindView(R.id.factorProductLlist) RecyclerView mProductList;

    @BindView(R.id.createFactorCustomer) LinearLayout mAddCustomer; // Clickable
    @BindView(R.id.createFactorCustomerTxt) TextView mCustomerTxt;
    @BindView(R.id.createFactorCost) TextView mFactorPrice;

    int dim8;

    public int NEW_FACTOR_ID = 0;
    private String CUSTOMER_NAME = "";
    private String CUSTOMER_PHONE = "";
    private int FACTOR_PRICE = 0;

    public CreateFactorContent(Activity activity, int type, CreateFactorViewModel viewModel){
        this.mViewModel = viewModel;
        ButterKnife.bind(this, activity);
        dim8 = activity.getResources().getDimensionPixelSize(R.dimen.dimen8);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.IranBold(activity));
        mCustomerTxt.setTypeface(Fonts.IranBold(activity));
        mFactorPrice.setTypeface(Fonts.IranBold(activity));
        mFactorPrice.setText("0");
        SoldProducts.clear();
        SoldProducts.setList(new ArrayList<>());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mNewProduct.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mAddProduct.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mBarcodeReader.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mAddCustomer.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mStoreFactor.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
        }

        {
            switch (type){
                case 0:
                    mTitle.setText(R.string.rejected_factor);
                    break;
                case 1:
                    mTitle.setText(R.string.purchase_factor);
                    break;
                case 2:
                    mTitle.setText(R.string.sale_factor);
                    break;
            }
        }


        viewModel.getFactors().observe(CreateFactorActivity.getInstance(), factorModels -> {
            if (factorModels.isEmpty()){
                NEW_FACTOR_ID = 0;
            } else {
                NEW_FACTOR_ID = GenerateFactorID.newID_Ascending(factorModels);
            }
        });






        // create new product
        mNewProduct.setOnClickListener(v -> {
            Intent intent = new Intent(activity, NewProductActivity.class);
            intent.putExtra("new_product_type", 2);
            activity.startActivityForResult(intent, 1);
        });
        // find & get an existing product
        mAddProduct.setOnClickListener(v -> {
            Intent intent = new Intent(activity, StorageActivity.class);
            intent.putExtra("storage_type", 2);
            activity.startActivityForResult(intent, 1);
        });
        // add a product with barcode reader
        mBarcodeReader.setOnClickListener(v -> {
            MyToast.error(activity.getString(R.string.in_development)).show();
        });
        mAddCustomer.setOnClickListener(v -> {
            Intent intent = new Intent(activity, CustomerName.class);
            activity.startActivityForResult(intent, 2);
        });
        // --- create new product
        mStoreFactor.setOnClickListener(v -> {
            if (FACTOR_PRICE == 0){
                MyToast.warning(activity.getString(R.string.factors_is_empty)).show();
            } else {
                createFactor(
                        activity,
                        NEW_FACTOR_ID,
                        String.valueOf(NEW_FACTOR_ID),
                        System.currentTimeMillis(),
                        CUSTOMER_NAME,
                        CUSTOMER_PHONE,
                        type,
                        FACTOR_PRICE
                );
            }
        });
    }
    public void setCustomerName(String name, String phone){
        mCustomerTxt.setText(name);
        CUSTOMER_NAME = name;
        CUSTOMER_PHONE = phone;
    }
    public void setFactorPrice(int cost){
        mFactorPrice.setText(ConvertNumbers.convertToPersian(String.valueOf(cost), true));
        FACTOR_PRICE = cost;
    }
    public void setProductList(Activity context, List<SoldProductModel> soldProductModel){
        mSoldProductsModel = soldProductModel;

        FactorsSoldProductsAdapter adapter = new FactorsSoldProductsAdapter(context, soldProductModel, this, mViewModel);
        mProductList.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new FactorsSoldAdapterSwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(mProductList);
        adapter.notifyDataSetChanged();
        mProductList.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));


        int cost = 0;
        for (int i = 0; i < soldProductModel.size(); i++){
            cost = cost + (soldProductModel.get(i).getSoldAmount()*soldProductModel.get(i).getSoldSellingPrice());
        }

        setFactorPrice(cost);
        MyLog.LogDebugging("CreateFactorActivity() ---> " + "onActivityResult() ---> " + "setProductList() ---> " + "| DONE");
        context.setResult(0);
    }
    public void updateFactorPrice(List<SoldProductModel> soldProductModel){
        int cost = 0;
        for (int i = 0; i < soldProductModel.size(); i++){
            cost = cost + (soldProductModel.get(i).getSoldAmount()*soldProductModel.get(i).getSoldSellingPrice());
        }
        setFactorPrice(cost);
    }
    private void createFactor(Activity activity, int factorID, String factorReferenceCode, long date, String customerName, String customerPhone, int type, int cost){
        MyLog.LogDebugging("CreateFactorActivity() ---> " + "CreateFactorContent() ---> " + "createFactor()" + "\n" +
                "FACTOR_ID = " + factorID + "\n" +
                "REFERENCE_ID = " + factorReferenceCode + "\n" +
                "DATE = " + date + "\n" +
                "CUSTOMER_NAME = " + customerName + "\n" +
                "CUSTOMER_PHONE = " + customerPhone + "\n" +
                "FACTOR_STATUS = " + 1 + "\n" +
                "USER_ID = " + MySharedPreferences.getInteger(MyPreferences.USER_ID, 0) + "\n" +
                "FACTOR_TYPE = " + type + "\n" +
                "PRODUCT_COUNT = " + 0 + "\n" +
                "FACTOR_COST = " + cost);
        mViewModel.insertFactor(activity, new FactorModel(
                factorID,
                factorReferenceCode,
                date,
                customerName,
                customerPhone,
                1,
                MySharedPreferences.getInteger(MyPreferences.USER_ID, 0),
                type,
                0,
                cost
        ), this);
    }

    public void clearEntries(Activity context) {
        MyLog.LogDebugging("CreateFactorActivity() ---> " + "CreateFactorContent() ---> " + "clearEntries()");
        // TODO Clear Everything When Factor is Created
        context.finish();
    }

    public void backPresses(Activity activity) {
        if (FACTOR_PRICE == 0){
            activity.finish();
        } else {
            showFactorRemoveWarningDialog(activity);
        }
    }
    private void showFactorRemoveWarningDialog(Activity activity){
        AlertDialog alertDialog = new AlertDialog.Builder(activity, R.style.AlertDialogCustom).create();
        alertDialog.setTitle(activity.getString(R.string.delete_factor));
        alertDialog.setMessage(activity.getString(R.string.do_u_want_factor_removed));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, activity.getString(R.string.yes),
                (dialog, which) -> {
                    dialog.dismiss();
                    activity.finish();
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, activity.getString(R.string.no),
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
        try {

            TextView messageText = (TextView) Objects.requireNonNull(alertDialog.getWindow()).findViewById(android.R.id.message);
            TextView positiveButton = (TextView) alertDialog.getWindow().findViewById(android.R.id.button1);
            TextView negativeButton = (TextView) alertDialog.getWindow().findViewById(android.R.id.button2);
            messageText.setTypeface(Fonts.IranNormal(activity));
            messageText.setTextSize(14);
            positiveButton.setTypeface(Fonts.IranBold(activity));
            positiveButton.setTextSize(14);
            negativeButton.setTypeface(Fonts.IranBold(activity));
            negativeButton.setTextSize(14);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                positiveButton.setForeground(ContextCompat.getDrawable(activity, R.drawable.ripple_accent));
                negativeButton.setForeground(ContextCompat.getDrawable(activity, R.drawable.ripple_accent));
            }
            ViewGroup.MarginLayoutParams positiveParams = (ViewGroup.MarginLayoutParams) positiveButton.getLayoutParams();
            positiveParams.setMargins(dim8,dim8,dim8,0);
            positiveButton.setLayoutParams(positiveParams);
            ViewGroup.MarginLayoutParams negativeParams = (ViewGroup.MarginLayoutParams) positiveButton.getLayoutParams();
            negativeParams.setMargins(dim8,dim8,dim8,0);
            negativeButton.setLayoutParams(negativeParams);
            TextView titleText = (TextView) alertDialog.findViewById(android.R.id.title);
            titleText.setTypeface(Fonts.IranBold(activity));
            titleText.setTextSize(15);
        } catch (NullPointerException f){
            f.printStackTrace();
        }
    }

}
