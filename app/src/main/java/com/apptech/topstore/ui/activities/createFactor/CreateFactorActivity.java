package com.apptech.topstore.ui.activities.createFactor;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.apptech.topstore.AppController;
import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.soldProducts.SoldProductModel;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.ui.activities.createFactor.contents.CreateFactorContent;
import com.apptech.topstore.utils.MyLog;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CreateFactorActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    private CreateFactorViewModel viewModel;
    CreateFactorContent createFactorContent;


    private static CreateFactorActivity instance;
    public static CreateFactorActivity getInstance() {
        return instance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_factor);
        instance = this;
        setWindowDecor();


        viewModel = ViewModelProviders.of(this).get(CreateFactorViewModel.class);
        AppController.get(this).getAppComponent().MyInject(viewModel);
        createFactorContent = new CreateFactorContent(this, getIntent().getIntExtra("factor_type", 2), viewModel);
    }
    private void setWindowDecor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(),R.color.fullTransparent));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            if (resultCode == 1){
                MyLog.LogDebugging("CreateFactorActivity() ---> " + "onActivityResult() ---> " + "REQUEST_CODE " + 1 + " | RESULT_CODE = " + resultCode);

                if (SoldProducts.getList() != null){
                    List<SoldProductModel> soldProductsList = new ArrayList<>();
                    for (ProductModel productModel : SoldProducts.getList()) {
                        soldProductsList.add(new SoldProductModel(
                                createFactorContent.NEW_FACTOR_ID + " - " + productModel.getProductID(),
                                createFactorContent.NEW_FACTOR_ID,
                                productModel.getProductID(),
                                productModel.getProductUnitId(),
                                productModel.getProductAmount(),
                                productModel.getProductBasePrice(),
                                productModel.getProductSellingPrice()
                        ));

                        viewModel.insertSoldProducts(CreateFactorActivity.this,
                                new SoldProductModel(
                                        createFactorContent.NEW_FACTOR_ID + " - " + productModel.getProductID(),
                                        createFactorContent.NEW_FACTOR_ID,
                                        productModel.getProductID(),
                                        productModel.getProductUnitId(),
                                        productModel.getProductAmount(),
                                        productModel.getProductBasePrice(),
                                        productModel.getProductSellingPrice()
                                ),
                                createFactorContent);
                    }

                    createFactorContent.setProductList(this, soldProductsList);
                }

            } else {
                MyLog.LogDebugging("CreateFactorActivity() ---> " + "onActivityResult() ---> " + "REQUEST_CODE " + 1 + " | RESULT_CODE = " + null);
                // nothing to add
            }
        } else if (requestCode == 2){
            if (resultCode == 1){
                MyLog.LogDebugging("CreateFactorActivity() ---> " + "onActivityResult() ---> " + "REQUEST_CODE " + 2 + " | RESULT_CODE = " + resultCode);

                if (data != null){
                    createFactorContent.setCustomerName(
                            data.getStringExtra("customer_name"),
                            data.getStringExtra("customer_phone")
                    );
                }

            } else {
                MyLog.LogDebugging("CreateFactorActivity() ---> " + "onActivityResult() ---> " + "REQUEST_CODE " + 2 + " | RESULT_CODE = " + null);
                // nothing to add
            }
        } else {
            MyLog.LogDebugging("CreateFactorActivity() ---> " + "onActivityResult() ---> " + "REQUEST_CODE ELSE");
        }
    }

    @Override
    protected void onDestroy() {
        MyLog.LogDebugging("CreateFactorActivity() ---> " + "onDestroy()");
        if (viewModel.disposable != null){
            viewModel.disposable.dispose();
        }
        super.onDestroy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        createFactorContent.backPresses(CreateFactorActivity.this);
    }
}
