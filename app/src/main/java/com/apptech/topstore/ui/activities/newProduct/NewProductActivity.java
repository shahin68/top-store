package com.apptech.topstore.ui.activities.newProduct;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.apptech.topstore.AppController;
import com.apptech.topstore.R;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.ui.activities.newProduct.contents.NewProductContent;
import com.apptech.topstore.utils.MyLog;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NewProductActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private NewProductViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_product);
        setWindowDecor();



        viewModel = ViewModelProviders.of(this).get(NewProductViewModel.class);
        AppController.get(this).getAppComponent().MyInject(viewModel);
        new NewProductContent(NewProductActivity.this, viewModel, getIntent().getIntExtra("new_product_type", 0));


    }



    private void setWindowDecor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(),R.color.fullTransparent));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }


    @Override
    protected void onDestroy() {
        MyLog.LogDebugging("NewProductActivity() ---> " + "onDestroy()");
        if (viewModel.disposable != null){
            viewModel.disposable.dispose();
        }
        super.onDestroy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
