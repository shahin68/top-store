package com.apptech.topstore.ui.activities.storage.storageData;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.R;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.databaseLocal.Unit;
import com.apptech.topstore.ui.activities.storage.StorageActivity;
import com.apptech.topstore.ui.activities.storage.StorageViewModel;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StorageAdapter extends RecyclerView.Adapter<StorageAdapter.myViewHolder>{
    List<ProductModel> items = new ArrayList<>();
    private Context mContext;
    private LayoutInflater inflater;
    private StorageViewModel mViewModel;

    public Context getContext() {
        return mContext;
    }
    String mSearchPhrase;

    public StorageAdapter(Context context, List<ProductModel> arrayList, String searchPhrase, StorageViewModel viewModel){
        this.mContext = context;
        this.items = arrayList;
        inflater = LayoutInflater.from(context);
        this.mSearchPhrase = searchPhrase;
        this.mViewModel = viewModel;

        Collections.sort(items, (o1, o2) -> {
            if ((o1.getProductName()+"-"+ o1.getProductAmount() +"-"+ o1.getProductSellingPrice() +"-"+Unit.getUnit(mContext, o1.getProductUnitId())).contains(latinNumOf(searchPhrase)) && !(o2.getProductName()+"-"+ o2.getProductAmount() +"-"+ o2.getProductSellingPrice() +"-"+Unit.getUnit(mContext, o2.getProductUnitId())).contains(latinNumOf(searchPhrase))){
                return -1;
            } else if (!(o1.getProductName()+"-"+ o1.getProductAmount() +"-"+ o1.getProductSellingPrice() +"-"+Unit.getUnit(mContext, o1.getProductUnitId())).contains(latinNumOf(searchPhrase)) && (o2.getProductName()+"-"+ o2.getProductAmount() +"-"+ o2.getProductSellingPrice() +"-"+Unit.getUnit(mContext, o2.getProductUnitId())).contains(latinNumOf(searchPhrase))){
                return 1;
            } else if ((o1.getProductName()+"-"+ o1.getProductAmount() +"-"+ o1.getProductSellingPrice() +"-"+Unit.getUnit(mContext, o1.getProductUnitId())).contains(latinNumOf(searchPhrase)) && (o2.getProductName()+"-"+ o2.getProductAmount() +"-"+ o2.getProductSellingPrice() +"-"+Unit.getUnit(mContext, o2.getProductUnitId())).contains(latinNumOf(searchPhrase))){
                return 0;
            } else {
                return (o1.getProductName()+"-"+ o1.getProductAmount() +"-"+ o1.getProductSellingPrice() +"-"+Unit.getUnit(mContext, o1.getProductUnitId())).compareToIgnoreCase(o2.getProductName()+"-"+ o2.getProductAmount() +"-"+ o2.getProductSellingPrice() +"-"+Unit.getUnit(mContext, o2.getProductUnitId()));
            }
        });

    }

    private String latinNumOf(String num) {
        return ConvertNumbers.convertToLatin(num);
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView = inflater.inflate(R.layout.item_storage, parent, false);
        return new myViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {



        if (!mSearchPhrase.isEmpty()){
            holder.mTitle.setText(Html.fromHtml(ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getProductName()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>" + latinNumOf(mSearchPhrase) + "</font>"), false)));
            holder.mAmount.setText(Html.fromHtml(ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getProductAmount() + " " + Unit.getUnit(mContext, items.get(position).getProductUnitId())).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>"+latinNumOf(mSearchPhrase)+"</font>"), false)));
            holder.mOneUnitPrice.setText(Html.fromHtml("<html><body>" + String.valueOf(items.get(position).getProductSellingPrice()).replaceAll(latinNumOf(mSearchPhrase), "<font color='red'>"+latinNumOf(mSearchPhrase)+"</font>") + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));


            if (!(items.get(position).getProductName()+"-"+ String.valueOf(items.get(position).getProductAmount()) +"-"+ String.valueOf(items.get(position).getProductSellingPrice()) +"-"+Unit.getUnit(mContext, items.get(position).getProductUnitId())).contains(latinNumOf(mSearchPhrase))){
                holder.itemView.setAlpha(.3f);
            } else {
                holder.itemView.setAlpha(1);
            }
        } else {
            holder.mTitle.setText(ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getProductName()), false));
            holder.mAmount.setText(ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getProductAmount()), false) + " " + Unit.getUnit(mContext, items.get(position).getProductUnitId()));
            holder.mOneUnitPrice.setText(Html.fromHtml("<html><body>" + ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getProductSellingPrice()), true) + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));

            holder.itemView.setAlpha(1);
        }




    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class myViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.itemStorageProduct) TextView mTitle;
        @BindView(R.id.itemStorageAmount) TextView mAmount;
        @BindView(R.id.itemStorageUnitPrice) TextView mOneUnitPrice;



        public myViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mTitle.setTypeface(Fonts.IranBold(mContext));
            mAmount.setTypeface(Fonts.IranBold(mContext));
            mOneUnitPrice.setTypeface(Fonts.IranBold(mContext));


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                itemView.setForeground(ContextCompat.getDrawable(mContext,R.drawable.ripple_accent_general));
            }




            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

        }
    }

    // TODO FIX THIS LATER
    public void deleteItem(int position) {
//        showUndoSnackBar(position, items.get(position));
        mViewModel.delete(items.get(position));
        items.remove(position);
        notifyItemRemoved(position);
    }
    private void showUndoSnackBar(int position, ProductModel productModel) {
        View view = StorageActivity.getInstance().findViewById(R.id.storage_layout);
        Snackbar snackbar = Snackbar.make(view, mContext.getString(R.string.product_removed), Snackbar.LENGTH_LONG);
        snackbar.setAction(mContext.getString(R.string.undo_remove), v -> undoDelete(position, productModel));
        snackbar.show();
        snackbar.addCallback(new Snackbar.Callback(){
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                mViewModel.delete(productModel);
            }
        });
    }
    private void undoDelete(int position, ProductModel productModel) {
        items.add(position, productModel);
        notifyItemInserted(position);
    }
}
