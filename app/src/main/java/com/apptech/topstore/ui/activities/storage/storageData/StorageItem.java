package com.apptech.topstore.ui.activities.storage.storageData;

import java.util.ArrayList;

public class StorageItem {
    public static ArrayList<StorageItem> storageItems = new ArrayList<>();
    public String title;
    public int amount;
    public int oneUnitPrice;
    public String unit;
}
