package com.apptech.topstore.ui.activities.factors.factorsData;

import java.util.ArrayList;

public class FactorsItem {
    public static ArrayList<FactorsItem> factorsItems = new ArrayList<>();
    public int factorId;
    public int factorNumber;
    public int productsCount;
    public String date;
    public int basePrice;
    public int shopPrice;
    public int status;
}
