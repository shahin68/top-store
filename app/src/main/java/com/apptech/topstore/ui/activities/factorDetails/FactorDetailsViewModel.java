package com.apptech.topstore.ui.activities.factorDetails;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.apptech.topstore.database.AppDatabase;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.database.factors.soldProducts.SoldProductModel;
import com.apptech.topstore.database.products.ProductModel;


import java.util.List;

import javax.inject.Inject;

public class FactorDetailsViewModel extends ViewModel {
    @Inject
    AppDatabase appDatabase;

    public LiveData<ProductModel> getProducts(int id) {
        return appDatabase.productsDao().findProductByID(id);
    }

    public LiveData<FactorModel> getFactorByID(int factorID) {
        return appDatabase.factorsDao().findFactorByID(factorID);
    }

    public LiveData<List<SoldProductModel>> getSoldProducts() {
        return appDatabase.soldProductDao().getAllLive();
    }

    public LiveData<List<SoldProductModel>> getSoldProductsByFactorID(int id) {
        return appDatabase.soldProductDao().findSoldProductsByFactorID(id);
    }


    public void updateFactor(FactorModel factorModel){
        new updateTask().execute(factorModel);
    }
    private class updateTask extends AsyncTask<FactorModel, Void, Void>{
        @Override
        protected Void doInBackground(FactorModel... factorModels) {
            appDatabase.factorsDao().updateOne(factorModels[0]);
            return null;
        }
    }

    public void delete(SoldProductModel soldProductModel){
        new deleteTask().execute(soldProductModel);
    }
    private class deleteTask extends AsyncTask<SoldProductModel, Void, Void> {
        @Override
        protected Void doInBackground(SoldProductModel... soldProductModels) {
            appDatabase.soldProductDao().delete(soldProductModels[0]);
            return null;
        }
    }
}
