package com.apptech.topstore.ui.activities.factors.contents;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.ui.activities.factors.FactorsViewModel;
import com.apptech.topstore.ui.activities.factors.factorsData.FactorsAdapter;
import com.apptech.topstore.ui.activities.factors.factorsData.FactorsAdapterSwipeToDeleteCallback;
import com.apptech.topstore.ui.activities.factors.factorsData.FactorsFakeData;
import com.apptech.topstore.ui.activities.factors.factorsData.FactorsItem;
import com.apptech.topstore.utils.Fonts;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FactorsContent {
    FactorsViewModel mViewModel;
    List<FactorModel> mFactorModels;
    InputMethodManager inputMethodManager;

    // --- toolbar views
    @BindView(R.id.factorsTitle) TextView mTitle;
    @BindView(R.id.factorsBack) LinearLayout mBack;
    @BindView(R.id.factorsTopDecor) AppCompatImageView mTopDecor;

    // --- list
    @BindView(R.id.factors_list) RecyclerView mList;

    // --- Search Views
    @BindView(R.id.factorsSearchInput) TextInputEditText mSearchInput;
    @BindView(R.id.factorsSearchIcon) AppCompatImageView mSearchIcon;
    @BindView(R.id.factorsSearchFilter) AppCompatImageView mFilterButton; // Clickable


    public FactorsContent(Activity activity, FactorsViewModel factorsViewModel, List<FactorModel> factorModels){
        mViewModel = factorsViewModel;
        ButterKnife.bind(this, activity);
        inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.IranBold(activity));
        mSearchInput.setTypeface(Fonts.IranBold(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mSearchIcon.setBackground(ContextCompat.getDrawable(activity, R.drawable.ripple_round_accent_48dp));
            mFilterButton.setBackground(ContextCompat.getDrawable(activity, R.drawable.ripple_round_accent_48dp));
        }

        mFilterButton.setOnClickListener(view -> setFilter());
        setSearchInputMethod(activity);
        mSearchIcon.setOnClickListener(v -> searchIconClick(activity));
        searchInputTextWatcher(activity);
        initFactorsList(activity, factorModels);
    }
    public void initFactorsList(Context context, List<FactorModel> factorModels){
        /*if (!FactorsItem.factorsItems.isEmpty()) {
            FactorsItem.factorsItems.clear();
        }
        for (int i = 0; i < FactorsFakeData.Count(); i++){
            FactorsItem factorsItem = new FactorsItem();
            factorsItem.factorNumber = FactorsFakeData.numbers(i);
            factorsItem.status = FactorsFakeData.status(i);
            factorsItem.date = FactorsFakeData.dates(i);
            factorsItem.shopPrice = FactorsFakeData.price(i);
            factorsItem.productsCount = FactorsFakeData.counts(i);
            FactorsItem.factorsItems.add(factorsItem);
        }*/
        mFactorModels = factorModels;

        FactorsAdapter adapter = new FactorsAdapter(context, factorModels, "", mViewModel);
        mList.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new FactorsAdapterSwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(mList);
        adapter.notifyDataSetChanged();
        mList.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
    }
    private void search (Context context, String searchPhrase) {
        if (searchPhrase.isEmpty()){
            FactorsAdapter adapter = new FactorsAdapter(context, mFactorModels, "", mViewModel);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new FactorsAdapterSwipeToDeleteCallback(adapter));
            itemTouchHelper.attachToRecyclerView(mList);
            mList.swapAdapter(adapter, false);
            mSearchInput.getText().clear();
        } else if (!searchPhrase.isEmpty()){
            FactorsAdapter adapter = new FactorsAdapter(context, mFactorModels, searchPhrase, mViewModel);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new FactorsAdapterSwipeToDeleteCallback(adapter));
            itemTouchHelper.attachToRecyclerView(mList);
            mList.swapAdapter(adapter, false);
        }
    }
    private void setFilter() {
        /*if (SharedPreferencesCarfix.getBoolean(Preferences.USER_REGISTERED, false)){
                    if (!ProductsCategoryItem.productsCategoryItems.isEmpty()){
                        PopupMenu popup = new PopupMenu(getActivity(), view);
                        showPopupMenuIcons(popup);
                        int count = ProductsCategoryItem.productsCategoryItems.size();
                        for (int i = 0; i < count; i++){
                            popup.getMenu().add(1,
                                    i,
                                    i,
                                    ProductsCategoryItem.productsCategoryItems.get(i).categoryTitle)
                                    .setIcon(R.drawable.ic_wrench)
                                    .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
                        }
                        popup.setOnMenuItemClickListener(
                                new PopupMenu.OnMenuItemClickListener() {
                                    public boolean onMenuItemClick(MenuItem item) {
//                                    search(item.getTitle().toString());
                                        new searchSync().execute(item.getTitle().toString());
                                        return true;
                                    }
                                });
                        popup.show();
                    }
                } else {
                    // --- Local Data For Testing
                    PopupMenu popup = new PopupMenu(getActivity(), view);
                    showPopupMenuIcons(popup);
                    int count = ProductsCategoryLocalData.Count();
                    for (int i = 0; i < count; i++){
                        popup.getMenu().add(1,
                                i,
                                i,
                                ProductsCategoryLocalData.title(i))
                                .setIcon(ProductsCategoryLocalData.iconDrawable(i))
                                .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
                    }
                    popup.setOnMenuItemClickListener(
                            new PopupMenu.OnMenuItemClickListener() {
                                public boolean onMenuItemClick(MenuItem item) {
//                                    search(item.getTitle().toString());
                                    new searchSync().execute(item.getTitle().toString());
                                    return true;
                                }
                            });
                    popup.show();
                }*/
    }
    private void setSearchInputMethod(Activity activity) {
        mSearchInput.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.KEYCODE_SEARCH) ||
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {

                // Perform action on key press
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    try {
                        inputMethodManager.hideSoftInputFromWindow(Objects.requireNonNull(activity.getWindow().getCurrentFocus()).getWindowToken(), 0);
                    } catch (NullPointerException f){
                        f.printStackTrace();
                    }
                }
                search(activity, mSearchInput.getText().toString());
                return true;
            }
            return false;
        });
    }
    private void searchInputTextWatcher(Activity context) {
        mSearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()){
                    if (mSearchIcon.getRotation() != 0) {
                        closeIconToSearchOnly(context);
                        search(context, "");
                    }
                } else {
                    if (mSearchIcon.getRotation() == 0){
                        searchIconToClose(context);
                    }
                }
            }
        });
    }
    private void searchIconClick(Activity activity) {
        if (mSearchIcon.getRotation() == 0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                try {
                    // TODO Add if Statement for when keyboard isn't showing to open it
                    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                } catch (NullPointerException f){
                    f.printStackTrace();
                }
            }
            searchIconToClose(activity);
        } else {
            search(activity,"");
            closeIconToSearch(activity);
        }
    }

    private void searchIconToClose(Context context) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, mSearchIcon.getRotation(), 180);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mSearchIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_close));
                ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, 180, 360);
                animator.setInterpolator(new DecelerateInterpolator(2f));
                animator.setDuration(300).start();
            }
        });
        animator.setInterpolator(new AccelerateInterpolator(2f));
        animator.setDuration(300).start();
    }
    private void closeIconToSearch(Activity activity) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, mSearchIcon.getRotation(), 180);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mSearchIcon.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_search));
                ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, 180, 0);
                animator.setInterpolator(new DecelerateInterpolator(2f));
                animator.setDuration(300).start();
            }
        });
        animator.setInterpolator(new AccelerateInterpolator(2f));
        animator.setDuration(300).start();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                inputMethodManager.hideSoftInputFromWindow(Objects.requireNonNull(activity.getWindow().getCurrentFocus()).getWindowToken(), 0);
            } catch (NullPointerException f){
                f.printStackTrace();
            }
        }
    }
    private void closeIconToSearchOnly(Activity activity) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, mSearchIcon.getRotation(), 180);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mSearchIcon.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_search));
                ObjectAnimator animator = ObjectAnimator.ofFloat(mSearchIcon, View.ROTATION, 180, 0);
                animator.setInterpolator(new DecelerateInterpolator(2f));
                animator.setDuration(300).start();
            }
        });
        animator.setInterpolator(new AccelerateInterpolator(2f));
        animator.setDuration(300).start();
    }

}
