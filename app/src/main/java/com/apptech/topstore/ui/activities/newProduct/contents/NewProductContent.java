package com.apptech.topstore.ui.activities.newProduct.contents;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;

import com.apptech.topstore.R;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.databaseLocal.Unit;
import com.apptech.topstore.ui.activities.createFactor.SoldProducts;
import com.apptech.topstore.ui.activities.newProduct.NewProductViewModel;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewProductContent {
    private NewProductViewModel mViewModel;

    // --- toolbar views
    @BindView(R.id.newProductTitle) TextView mTitle;
    @BindView(R.id.newProductBack) LinearLayout mBack;
    @BindView(R.id.newProductTopDecor) AppCompatImageView mTopDecor;

    // ---- Content Views
    @BindView(R.id.newProductNameInput) TextInputEditText mProductNameInput;
    @BindView(R.id.newProductDefaultPriceInput) TextInputEditText mProductPriceInput;
    @BindView(R.id.newProductBarcodeInput) TextInputEditText mProductBarcodeInput;
    @BindView(R.id.newProductBarcodeImg) AppCompatImageView mProductBarcodeImg;
    @BindView(R.id.newProductUnitTxt) TextView mProductUnitTxt;
    @BindView(R.id.newProductInfoInput) TextInputEditText mProductInfoInput;
    @BindView(R.id.newProductUnit) CardView mChooseUnit;

    // --- add new product button
    @BindView(R.id.newProductAddBtn) Button mAddProduct;

    int UNIT_ID = 0;

    public NewProductContent(Activity activity, NewProductViewModel viewModel, int type){
        this.mViewModel = viewModel;
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.IranBold(activity));

        mProductNameInput.setTypeface(Fonts.IranBold(activity));
        mProductPriceInput.setTypeface(Fonts.IranBold(activity));
        mProductBarcodeInput.setTypeface(Fonts.IranBold(activity));
        mProductUnitTxt.setTypeface(Fonts.IranBold(activity));
        mProductInfoInput.setTypeface(Fonts.IranBold(activity));

        mProductBarcodeImg.setBackground(ContextCompat.getDrawable(activity,R.drawable.ripple_round_accent_48dp));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mChooseUnit.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mAddProduct.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
        }

        mProductBarcodeImg.setOnClickListener(v -> {

        });
        mAddProduct.setOnClickListener(v -> {
            if (type == 2){
                createProductForType(
                        activity,
                        Integer.parseInt(mProductBarcodeInput.getText().toString()),
                        mProductNameInput.getText().toString(),
                        mProductInfoInput.getText().toString(),
                        Integer.parseInt(mProductPriceInput.getText().toString()),
                        Integer.parseInt(mProductPriceInput.getText().toString()),
                        UNIT_ID,
                        1,
                        1
                );
            } else {
                createProduct(
                        activity,
                        Integer.parseInt(mProductBarcodeInput.getText().toString()),
                        mProductNameInput.getText().toString(),
                        mProductInfoInput.getText().toString(),
                        Integer.parseInt(mProductPriceInput.getText().toString()),
                        Integer.parseInt(mProductPriceInput.getText().toString()),
                        UNIT_ID,
                        1,
                        1
                );
            }
        });

        mChooseUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUnitMenu(activity, mChooseUnit);
            }
        });
    }
    private void createProduct(Activity activity, int barcode, String name, String description, int basePrice, int sellingPrice, int unitID, int amount, int groupID){
        MyLog.LogDebugging("NewProductActivity() ---> " + "NewProductContent() ---> " + "createProduct()");
        mViewModel.insert(activity, new ProductModel(
                barcode,
                barcode,
                name,
                description,
                basePrice,
                sellingPrice,
                unitID,
                amount,
                groupID
        ), this);
    }
    private void createProductForType(Activity activity, int barcode, String name, String description, int basePrice, int sellingPrice, int unitID, int amount, int groupID){
        MyLog.LogDebugging("NewProductActivity() ---> " + "NewProductContent() ---> " + "createProduct()");
        mViewModel.insertForType(activity, new ProductModel(
                barcode,
                barcode,
                name,
                description,
                basePrice,
                sellingPrice,
                unitID,
                amount,
                groupID
        ), this);
    }
    public void clearEntries(Context context) {
        MyLog.LogDebugging("NewProductActivity() ---> " + "NewProductContent() ---> " + "clearEntries()");
        Objects.requireNonNull(mProductBarcodeInput.getText()).clear();
        Objects.requireNonNull(mProductNameInput.getText()).clear();
        Objects.requireNonNull(mProductInfoInput.getText()).clear();
        Objects.requireNonNull(mProductPriceInput.getText()).clear();
        mProductUnitTxt.setText(context.getString(R.string.choose));
        MyToast.good(context.getString(R.string.product_stored)).show();
    }
    public void finishActivity(Activity context, List<ProductModel> productModels) {
        MyLog.LogDebugging("NewProductActivity() ---> " + "NewProductContent() ---> " + "finishActivity()");
        MyToast.good(context.getString(R.string.product_stored)).show();
        Intent returnIntent = new Intent();
        SoldProducts.add(productModels);
//        returnIntent.putExtra("sold_product_added", 100);
        context.setResult(1, returnIntent);
        context.finish();
    }
    private void showUnitMenu(Activity activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Unit.units(activity).length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    Unit.units(activity)[i].second,
                    i,
                    Unit.units(activity)[i].first);
        }
        popup.setOnMenuItemClickListener(
                item -> {
                    UNIT_ID = item.getItemId();
                    mProductUnitTxt.setText(item.getTitle());
                    return true;
                });
        popup.show();
    }
}
