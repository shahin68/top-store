package com.apptech.topstore.ui.activities.factorDetails;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.apptech.topstore.AppController;
import com.apptech.topstore.R;
import com.apptech.topstore.ui.activities.factorDetails.contents.FactorDetailsContent;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FactorDetailsActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private FactorDetailsViewModel viewModel;
    FactorDetailsContent factorsContent;

    private static FactorDetailsActivity instance;
    public static FactorDetailsActivity getInstance() {
        return instance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factor_details);
        instance = this;
        setWindowDecor();



        viewModel = ViewModelProviders.of(this).get(FactorDetailsViewModel.class);
        AppController.get(this).getAppComponent().MyInject(viewModel);


        viewModel.getSoldProductsByFactorID(getIntent().getIntExtra("factor_ID", 0)).observe(this, soldProductModels -> {
            factorsContent = new FactorDetailsContent(this, viewModel, soldProductModels, getIntent().getIntExtra("factor_ID", 0));
        });


    }
    private void setWindowDecor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(),R.color.fullTransparent));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }







    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
