package com.apptech.topstore.ui.activities.home.navDrawer;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.apptech.topstore.R;
import com.apptech.topstore.ui.activities.aboutUs.AboutUsActivity;
import com.apptech.topstore.ui.activities.home.HomeActivity;
import com.apptech.topstore.ui.activities.initialSettings.SettingsInitialActivity;
import com.apptech.topstore.ui.activities.profile.ProfileActivity;
import com.apptech.topstore.ui.activities.settings.SettingsActivity;
import com.apptech.topstore.utils.Fonts;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.myViewHolder>{
    private ArrayList<DrawerItem> items = new ArrayList<>();
    private Context mContext;
    private LayoutInflater inflater;



    public DrawerAdapter(Context context, ArrayList arrayList){
        super();
        this.mContext = context;
        this.items = arrayList;
        inflater = LayoutInflater.from(context);

    }


    @Override
    public DrawerAdapter.myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView = inflater.inflate(R.layout.nav_drawer_item, parent, false);
        return new DrawerAdapter.myViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(final myViewHolder holder, int position) {
        holder.mNavTitle.setText(items.get(position).nav_title);
        try {
            holder.mNavImg.setImageDrawable(ContextCompat.getDrawable(mContext, items.get(position).nav_img));
        } catch (Resources.NotFoundException f){
            holder.mNavImg.setImageResource(items.get(position).nav_img);
        }
    }




    @Override
    public int getItemCount() {
        return items.size();
    }
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class myViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.nav_item_txt) TextView mNavTitle;
        @BindView(R.id.nav_item_img) AppCompatImageView mNavImg;


        public myViewHolder(final View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);

            mNavTitle.setTypeface(Fonts.IranBold(mContext));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mNavImg.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext,R.color.textDark)));
            } else {
                mNavImg.setColorFilter(ContextCompat.getColor(mContext,R.color.textDark));
            }


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                itemView.setForeground(ContextCompat.getDrawable(mContext,R.drawable.ripple_accent_general));
            }


            itemView.setOnClickListener(this);
        }



        @Override
        public void onClick(View v) {
            switch(getAdapterPosition()) {
                case 0:
                    Intent intent0 = new Intent(mContext, ProfileActivity.class);
                    intent0.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (intent0.resolveActivity(mContext.getPackageManager()) != null){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            try {
                                Bundle rBundle = ActivityOptionsCompat.makeClipRevealAnimation(v, 0,0,v.getWidth(),v.getHeight()).toBundle();
                                v.getContext().startActivity(intent0, rBundle);
                                HomeActivity.getInstance().overridePendingTransition(0, 0);
                            } catch (IllegalStateException s){
                                v.getContext().startActivity(intent0);
                                s.printStackTrace();
                            }
                            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    HomeActivity.getInstance().navDrawer.mDrawerLayout.closeDrawers();
                                }
                            }, 1000);
                        } else {
                            v.getContext().startActivity(intent0);
                            HomeActivity.getInstance().navDrawer.mDrawerLayout.closeDrawers();
                        }
                    }
                    break;
                case 1:
                    /*Intent intent1 = new Intent(mContext, Addresses.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent1.putExtra("addresses_status", true);
                    if (intent1.resolveActivity(mContext.getPackageManager()) != null){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            try {
                                Bundle rBundle = ActivityOptionsCompat.makeClipRevealAnimation(v, 0,0,v.getWidth(),v.getHeight()).toBundle();
                                v.getContext().startActivity(intent1, rBundle);
                                Home.getInstance().overridePendingTransition(0, 0);
                            } catch (IllegalStateException s){
                                v.getContext().startActivity(intent1);
                                s.printStackTrace();
                            }
                            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Home.getInstance().mDrawerLayout.closeDrawers();
                                }
                            }, 1000);
                        } else {
                            v.getContext().startActivity(intent1);
                            Home.getInstance().mDrawerLayout.closeDrawers();
                        }
                    }*/
                    break;
                case 2:
                    /*Intent intent2 = new Intent(mContext, CustomerClub.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (intent2.resolveActivity(mContext.getPackageManager()) != null){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            try {
                                Bundle rBundle = ActivityOptionsCompat.makeClipRevealAnimation(v, 0,0,v.getWidth(),v.getHeight()).toBundle();
                                v.getContext().startActivity(intent2, rBundle);
                                Home.getInstance().overridePendingTransition(0, 0);
                            } catch (IllegalStateException s){
                                v.getContext().startActivity(intent2);
                                s.printStackTrace();
                            }
                            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Home.getInstance().mDrawerLayout.closeDrawers();
                                }
                            }, 1000);
                        } else {
                            v.getContext().startActivity(intent2);
                            Home.getInstance().mDrawerLayout.closeDrawers();
                        }
                    }*/
                    break;
                case 3:
                    /*Intent intent3 = new Intent(mContext, SendSuggestions.class);
                    intent3.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (intent3.resolveActivity(mContext.getPackageManager()) != null){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            try {
                                Bundle rBundle = ActivityOptionsCompat.makeClipRevealAnimation(v, 0,0,v.getWidth(),v.getHeight()).toBundle();
                                v.getContext().startActivity(intent3, rBundle);
                                Home.getInstance().overridePendingTransition(0, 0);
                            } catch (IllegalStateException s){
                                v.getContext().startActivity(intent3);
                                s.printStackTrace();
                            }
                            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Home.getInstance().mDrawerLayout.closeDrawers();
                                }
                            }, 1000);
                        } else {
                            v.getContext().startActivity(intent3);
                            Home.getInstance().mDrawerLayout.closeDrawers();
                        }
                    }*/
                    break;
                case 4:
//                    SocialIntent.call(Home.getInstance(), Preferences.CONTACT_NUM);
                    break;
                case 5:
                    Intent intent5 = new Intent(mContext, SettingsActivity.class);
                    intent5.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (intent5.resolveActivity(mContext.getPackageManager()) != null){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            try {
                                Bundle rBundle = ActivityOptionsCompat.makeClipRevealAnimation(v, 0,0,v.getWidth(),v.getHeight()).toBundle();
                                v.getContext().startActivity(intent5, rBundle);
                                HomeActivity.getInstance().overridePendingTransition(0, 0);
                            } catch (IllegalStateException s){
                                v.getContext().startActivity(intent5);
                                s.printStackTrace();
                            }
                            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    HomeActivity.getInstance().navDrawer.mDrawerLayout.closeDrawers();
                                }
                            }, 1000);
                        } else {
                            v.getContext().startActivity(intent5);
                            HomeActivity.getInstance().navDrawer.mDrawerLayout.closeDrawers();
                        }
                    }
                    break;
                case 6:
                    /*try {
                        if(Home.getInstance().mDrawerLayout.isDrawerOpen(Gravity.RIGHT)){
                            Home.getInstance().mDrawerLayout.closeDrawers();
                        }
                        Home.getInstance().runTour(Home.getInstance().mCarNameButton, "انتخاب خودرو", "قبل از ثبت هر سفارش خودرو خود را انتخاب نمایید تا خدمات و محصولات مرتبط با آن خودرو را مشاهده نمایید");
                    } catch (Exception f){
                        f.printStackTrace();
                    }*/
                    break;
                case 7:
                    Intent intent7 = new Intent(mContext, AboutUsActivity.class);
                    intent7.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (intent7.resolveActivity(mContext.getPackageManager()) != null){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            try {
                                Bundle rBundle = ActivityOptionsCompat.makeClipRevealAnimation(v, 0,0,v.getWidth(),v.getHeight()).toBundle();
                                v.getContext().startActivity(intent7, rBundle);
                                HomeActivity.getInstance().overridePendingTransition(0, 0);
                            } catch (IllegalStateException s){
                                v.getContext().startActivity(intent7);
                                s.printStackTrace();
                            }
                            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    HomeActivity.getInstance().navDrawer.mDrawerLayout.closeDrawers();
                                }
                            }, 1000);
                        } else {
                            v.getContext().startActivity(intent7);
                            HomeActivity.getInstance().navDrawer.mDrawerLayout.closeDrawers();
                        }
                    }
                    break;
            }
        }
    }
}
