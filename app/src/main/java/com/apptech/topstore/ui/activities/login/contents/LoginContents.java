package com.apptech.topstore.ui.activities.login.contents;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.databaseLocal.Country;
import com.apptech.topstore.ui.activities.home.HomeActivity;
import com.apptech.topstore.ui.activities.initialSettings.SettingsInitialActivity;
import com.apptech.topstore.ui.activities.login.LoginActivity;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.bumptech.glide.Glide;
import com.chaos.view.PinView;
import com.google.android.material.textfield.TextInputEditText;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginContents {
    @BindView(R.id.login_logo)
    AppCompatImageView mLogo;
    @BindView(R.id.login_txt)
    TextView mTxt;
    @BindView(R.id.login_button)
    Button mButton;
    @BindView(R.id.login_phone)
    CardView mPhoneView;
    @BindView(R.id.login_pinview)
    LinearLayout mPinLayout;

    // --- phone & Pin views
    @BindView(R.id.country_code)
    LinearLayout mCountryCode; // Clickable
    @BindView(R.id.country_flag)
    AppCompatImageView mCountryFlag;
    @BindView(R.id.phone_txt)
    TextView mPhoneTxt;
    @BindView(R.id.phone_input)
    TextInputEditText mPhoneInput;
    @BindView(R.id.country_code_txt)
    TextView mCountryCodeTxt; // Clickable
    @BindView(R.id.pin_retry)
    LinearLayout mRetryPin; // Clickable
    @BindView(R.id.pin_remaining_time)
    TextView mPinRetryTime;
    @BindView(R.id.pinView)
    PinView mPinView;
    public LoginContents(Activity activity){
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.logo_blue).into(mLogo);
        mPinView.setAnimationEnable(true);
        mTxt.setTypeface(Fonts.IranBold(activity));
        mPhoneTxt.setTypeface(Fonts.IranBold(activity));
        mCountryCodeTxt.setTypeface(Fonts.IranBold(activity));
        mPhoneInput.setTypeface(Fonts.IranBold(activity));
        mButton.setTypeface(Fonts.IranBold(activity));
        mPinRetryTime.setTypeface(Fonts.IranBold(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mButton.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mCountryCode.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mRetryPin.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }

        mPinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mButton.setOnClickListener(v -> {
            if (mPhoneView.getVisibility() == View.VISIBLE){
                mButton.setText(activity.getString(R.string.enter));
                mPhoneView.setVisibility(View.GONE);
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPinLayout.setVisibility(View.VISIBLE);
                    }
                }, 100);
            } else {
                openInitialSettings(activity);
            }
        });

        mRetryPin.setOnClickListener(v -> {
            mButton.setText(activity.getString(R.string.receive_sms_code));
            mPinLayout.setVisibility(View.GONE);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPhoneView.setVisibility(View.VISIBLE);
                }
            }, 100);
        });

        mCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCountryMenu(activity, mCountryCode);
            }
        });
    }

    public void openInitialSettings(Activity activity){
        MyLog.LogDebugging("LoginActivity() ---> " + "openInitialSettings()");
        Intent intent = new Intent(activity, SettingsInitialActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        activity.finish();
    }
    public void openHomeActivity(Activity activity){
        MyLog.LogDebugging("LoginActivity() ---> " + "openHomeActivity()");
        Intent intent = new Intent(activity, HomeActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        activity.finish();
    }

    public void onBackPresses(LoginActivity activity){
        if (mPhoneView.getVisibility() != View.VISIBLE){
            mButton.setText(activity.getString(R.string.receive_sms_code));
            mPinLayout.setVisibility(View.GONE);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPhoneView.setVisibility(View.VISIBLE);
                }
            }, 100);
        } else {
            activity.pressedBack();
        }
    }

    private void showCountryMenu(Activity activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Country.countryNames().length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    ConvertNumbers.convertToPersian(Country.countryNames()[i].second, false))
                    .setIcon(Country.countryFlags()[i])
                    .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        popup.setOnMenuItemClickListener(
                item -> {
                    mCountryFlag.setImageDrawable(item.getIcon());
                    mCountryCodeTxt.setText(item.getTitle());
                    return true;
                });
        showPopupMenuIcons(popup);
        popup.show();
    }
    private void showPopupMenuIcons(PopupMenu popup){
        try {
            Field[] fields = popup.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popup);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
