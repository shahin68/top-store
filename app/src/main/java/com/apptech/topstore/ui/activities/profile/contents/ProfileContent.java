package com.apptech.topstore.ui.activities.profile.contents;

import android.app.Activity;
import android.os.Build;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.databaseLocal.Country;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileContent {
    // --- toolbar views
    @BindView(R.id.profileTitle) TextView mTitle;
    @BindView(R.id.profileBack) LinearLayout mBack;
    @BindView(R.id.profileTopDecor) AppCompatImageView mTopDecor;
    @BindView(R.id.profileStoreBtn) Button mStoreBtn;

    // --- Contents
    @BindView(R.id.profileStoreNameInput) TextInputEditText mStoreNameInput;
    @BindView(R.id.profileUserNameInput) TextInputEditText mUserNameInput;
    @BindView(R.id.profileStoreNameTxt) TextView mStoreNameTxt;
    @BindView(R.id.profileUserNameTxt) TextView mUserNameTxt;

    @BindView(R.id.profileCountry) CardView mCountryCard;
    @BindView(R.id.profileCountryTxt) TextView mCountryTxt;
    @BindView(R.id.profileCity) CardView mCityCard;
    @BindView(R.id.profileCityTxt) TextView mCityTxt;
    @BindView(R.id.profileLocation) CardView mLocationCard;
    @BindView(R.id.profileLocationTxt) TextView mLocationTxt;
    @BindView(R.id.profileLocationChooseTxt) TextView mLocationChooseTxt;

    // --- phone
    @BindView(R.id.profileCountryCode) LinearLayout mCountryCode; // Clickable
    @BindView(R.id.profileCountryCodeTxt) TextView mCountryCodeTxt;
    @BindView(R.id.profileCountryCodeFlag) AppCompatImageView mCountryFlag;
    @BindView(R.id.profilePhoneInput) TextInputEditText mPhoneInput;

    public ProfileContent(Activity activity){
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.IranBold(activity));
        mStoreBtn.setTypeface(Fonts.IranBold(activity));
        mStoreNameInput.setTypeface(Fonts.IranBold(activity));
        mUserNameInput.setTypeface(Fonts.IranBold(activity));
        mStoreNameTxt.setTypeface(Fonts.IranBold(activity));
        mUserNameTxt.setTypeface(Fonts.IranBold(activity));
        mCountryTxt.setTypeface(Fonts.IranBold(activity));
        mCityTxt.setTypeface(Fonts.IranBold(activity));
        mLocationTxt.setTypeface(Fonts.IranBold(activity));
        mLocationChooseTxt.setTypeface(Fonts.IranBold(activity));
        mPhoneInput.setTypeface(Fonts.IranBold(activity));
        mCountryCodeTxt.setTypeface(Fonts.IranBold(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mStoreBtn.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mCountryCard.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mCityCard.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mLocationCard.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mCountryCode.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }

        mCountryCard.setOnClickListener(v -> {
            showCountryMenu(activity, mCountryCode);
        });
        mCityCard.setOnClickListener(v -> {});
        mLocationCard.setOnClickListener(v -> {});
        mCountryCode.setOnClickListener(v -> showCountryCodeMenu(activity, mCountryCode));
    }
    private void showCountryCodeMenu(Activity activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Country.countryNames().length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    ConvertNumbers.convertToPersian(Country.countryNames()[i].second, false))
                    .setIcon(Country.countryFlags()[i])
                    .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        popup.setOnMenuItemClickListener(
                item -> {
                    mCountryFlag.setImageDrawable(item.getIcon());
                    mCountryCodeTxt.setText(item.getTitle());
                    return true;
                });
        showPopupMenuIcons(popup);
        popup.show();
    }
    private void showCountryMenu(Activity activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Country.countryNames().length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    ConvertNumbers.convertToPersian(Country.countryNames()[i].first, false))
                    .setIcon(Country.countryFlags()[i])
                    .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        popup.setOnMenuItemClickListener(
                item -> {
                    mCountryFlag.setImageDrawable(item.getIcon());
                    mCountryCodeTxt.setText(item.getTitle());
                    return true;
                });
        showPopupMenuIcons(popup);
        popup.show();
    }
    private void showPopupMenuIcons(PopupMenu popup){
        try {
            Field[] fields = popup.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popup);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
