package com.apptech.topstore.ui.activities.reports;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.apptech.topstore.database.AppDatabase;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.database.factors.soldProducts.SoldProductModel;
import com.apptech.topstore.database.products.ProductModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class ReportsViewModel extends ViewModel {

    @Inject
    AppDatabase appDatabase;

    public LiveData<List<ProductModel>> getProducts() {
        return appDatabase.productsDao().getAllLive();
    }

    public LiveData<List<SoldProductModel>> getSoldProductsByFactorID(int id) {
        return appDatabase.soldProductDao().findSoldProductsByFactorID(id);
    }

    public LiveData<List<FactorModel>> getFactors() {
        return appDatabase.factorsDao().getAllFactorsLive();
    }

}
