package com.apptech.topstore.ui.activities.settings.contents;

import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.R;
import com.apptech.topstore.databaseLocal.Currency;
import com.apptech.topstore.databaseLocal.Languages;
import com.apptech.topstore.utils.Fonts;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsContent {
    // --- toolbar views
    @BindView(R.id.settingsTitle) TextView mTitle;
    @BindView(R.id.settingsBack) LinearLayout mBack;
    @BindView(R.id.settingsTopDecor) AppCompatImageView mTopDecor;

    // --- Content Views
    @BindView(R.id.settingsCurrency) CardView mCurrency;
    @BindView(R.id.settingsCalendar) CardView mCalendar;
    @BindView(R.id.settingsLanguage) CardView mLanguage;
    @BindView(R.id.settingsNotifications) CardView mNotifications;
    @BindView(R.id.settingsFactors) CardView mFactors;

    @BindView(R.id.settingsCurrencyTxt) TextView mCurrencyTxt;
    @BindView(R.id.settingsCalendarTxt) TextView mCalendarTxt;
    @BindView(R.id.settingsLanguageTxt) TextView mLanguageTxt;

    public SettingsContent(Activity activity){
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.IranBold(activity));


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mCurrency.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mCalendar.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mLanguage.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mNotifications.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mFactors.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }

        mCurrency.setOnClickListener(v -> {
            showCurrencyMenu(activity, v);
        });
        mLanguage.setOnClickListener(v -> {
            showLanguageMenu(activity, v);
        });
    }

    private void showCurrencyMenu(Activity activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Currency.countryCurrencies().length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    Currency.countryCurrencies()[i].first);
        }
        popup.setOnMenuItemClickListener(
                item -> {
                    mCurrencyTxt.setText(item.getTitle());
                    return true;
                });
        popup.show();
    }
    private void showLanguageMenu(Activity activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Languages.get().length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    Languages.get()[i]);
        }
        popup.setOnMenuItemClickListener(
                item -> {
                    mLanguageTxt.setText(item.getTitle());
                    return true;
                });
        popup.show();
    }
}
