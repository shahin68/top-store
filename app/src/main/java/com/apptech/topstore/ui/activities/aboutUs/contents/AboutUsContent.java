package com.apptech.topstore.ui.activities.aboutUs.contents;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.BuildConfig;
import com.apptech.topstore.R;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.SocialIntent;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutUsContent {
    // --- toolbar views
    @BindView(R.id.aboutUsTitle) TextView mTitle;
    @BindView(R.id.aboutUsBack) LinearLayout mBack;
    @BindView(R.id.aboutUsTopDecor) AppCompatImageView mTopDecor;
    @BindView(R.id.aboutUsAppVersion) TextView mAppVersion;

    // --- About Us Contents
    @BindView(R.id.aboutUsInfo) TextView mInfo;
    @BindView(R.id.aboutUsPhone) LinearLayout mPhone;
    @BindView(R.id.aboutUsPhoneTxt) TextView mPhoneTxt;
    @BindView(R.id.aboutUsWhatsApp) LinearLayout mWhatsApp;
    @BindView(R.id.aboutUsTelegram) LinearLayout mTelegram;

    public AboutUsContent(Activity activity){
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.IranBold(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mPhone.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mWhatsApp.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mTelegram.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }
        mAppVersion.setText(activity.getString(R.string.version) + " " + BuildConfig.VERSION_NAME);
    }
    public void setInfo(String info){
        mInfo.setText(info);
    }
    public void setPhone(Context context, String phone){
        mPhoneTxt.setText(ConvertNumbers.convertToPersian(phone, false));
        mPhone.setOnClickListener(v -> SocialIntent.call(context, phone));
    }

}
