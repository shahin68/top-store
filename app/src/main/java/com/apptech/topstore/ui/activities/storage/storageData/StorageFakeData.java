package com.apptech.topstore.ui.activities.storage.storageData;

public class StorageFakeData {
    public static Integer Count(){
        return 21;
    }
    public static int amount(int pick){
        int[] ints = new int[]{
                1844,
                485,
                40,
                30,
                857,
                245,
                125,
                288,
                312,
                99,
                454,
                75,
                624,
                288,
                46234,
                245,
                125,
                288,
                30,
                857,
                245
        };

        return ints[pick];
    }
    public static int unitPrice(int pick){
        int[] ints = new int[]{
                184400,
                485000,
                400000,
                30000,
                857000,
                245000,
                125900,
                28800,
                65000,
                9900,
                454000,
                485000,
                400000,
                30000,
                857000,
                245000,
                125900,
                28800,
                65000,
                485000,
                400000
        };

        return ints[pick];
    }
    public static String title(int pick){
        String[] ints = new String[]{
                "سیب",
                "گلابی",
                "شیر",
                "نان",
                "شکر",
                "قند",
                "نمک",
                "آرد",
                "تخم مرغ",
                "سیب زمینی",
                "مرغ",
                "کره",
                "گوشت",
                "آب معدنی",
                "نوشابه",
                "چیپس",
                "پفک",
                "ماکارونی",
                "سیگار",
                "تن ماهی",
                "ماست"
        };
        return ints[pick];
    }
    public static String unit(int pick){
        String[] ints = new String[]{
                "کیلو",
                "کیلو",
                "لیتر",
                "بسته",
                "کیلو",
                "کیلو",
                "کیلو",
                "کیلو",
                "شانه",
                "کیلو",
                "کیلو",
                "بسته",
                "کیلو",
                "بسته",
                "بطری",
                "عدد",
                "عدد",
                "بسته",
                "بسته",
                "عدد",
                "کیلو"
        };
        return ints[pick];
    }
}
