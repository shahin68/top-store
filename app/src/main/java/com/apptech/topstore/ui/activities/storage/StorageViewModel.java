package com.apptech.topstore.ui.activities.storage;

import android.app.Activity;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.apptech.topstore.R;
import com.apptech.topstore.database.AppDatabase;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.ui.activities.storage.contents.StorageContent;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class StorageViewModel extends ViewModel {
    @Inject
    AppDatabase appDatabase;
    Disposable disposable;

    public LiveData<List<ProductModel>> getProducts() {
        return appDatabase.productsDao().getAllLive();
    }

    public void delete(ProductModel productModel){
        new deleteTask().execute(productModel);
    }
    private class deleteTask extends AsyncTask<ProductModel, Void, Void> {
        @Override
        protected Void doInBackground(ProductModel... productModels) {
            appDatabase.productsDao().delete(productModels[0]);
            return null;
        }
    }

    public void insert(ProductModel productModel) { appDatabase.productsDao().insertOne(productModel); }

    public void insertForType(Activity context, List<ProductModel> productModel, StorageContent storageContent) {
        insertForTypeObservable(productModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Void>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        MyLog.LogDebugging("StorageActivity() ---> " + "insertForType() ---> " + "onSubscribe()");
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MyToast.error(context.getString(R.string.store_product_failed)).show();
                        MyLog.LogDebugging("StorageActivity() ---> " + "insertForType() ---> " + "onError()");
                    }

                    @Override
                    public void onComplete() {
                        storageContent.finishActivity(context, productModel);
                        MyLog.LogDebugging("StorageActivity() ---> " + "insertForType() ---> " + "onComplete()");
                    }
                });
    }
    private Observable<Void> insertForTypeObservable(List<ProductModel> productModel) {
        return new Observable<Void>() {
            @Override
            protected void subscribeActual(Observer<? super Void> observer) {
                appDatabase.productsDao().insertAll(productModel);
                observer.onSubscribe(new Disposable() {
                    @Override
                    public void dispose() {

                    }

                    @Override
                    public boolean isDisposed() {
                        return false;
                    }
                });
                observer.onComplete();
            }
        };
    }
}
