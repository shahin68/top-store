package com.apptech.topstore.ui.activities.splash.contents;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.BuildConfig;
import com.apptech.topstore.R;
import com.apptech.topstore.ui.activities.home.HomeActivity;
import com.apptech.topstore.ui.activities.login.LoginActivity;
import com.apptech.topstore.ui.activities.splash.SplashActivity;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashContent {
    @BindView(R.id.splash_background)
    AppCompatImageView mBackground;
    @BindView(R.id.splash_logo)
    AppCompatImageView mLogo;
    @BindView(R.id.splash_progressBar)
    ProgressBar mProgress;
    @BindView(R.id.splash_txt)
    TextView mText;


    public SplashContent(Activity activity){
        ButterKnife.bind(this, activity);
        mText.setTypeface(Fonts.IranBold(activity));
        mLogo.bringToFront();
        mProgress.bringToFront();
        mText.bringToFront();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mProgress.setIndeterminateTintList(ColorStateList.valueOf(ContextCompat.getColor(activity,R.color.white)));
        }


        Glide.with(activity).load(R.drawable.splash_background).into(mBackground);
        Glide.with(activity).load(R.drawable.logo_white).into(mLogo);
        mText.setText(activity.getString(R.string.version) + " " + BuildConfig.VERSION_NAME);
    }


    public void openLoginActivity(Activity activity){
        MyLog.LogDebugging("SplashActivity() ---> " + "openRegisterActivity()");
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        activity.finish();
    }
    public void openHomeActivity(Activity activity){
        MyLog.LogDebugging("SplashActivity() ---> " + "openHomeActivity()");
        Intent intent = new Intent(activity, HomeActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        activity.finish();
    }

}
