package com.apptech.topstore.ui.activities.createFactor.productsData;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.soldProducts.SoldProductModel;
import com.apptech.topstore.databaseLocal.Unit;
import com.apptech.topstore.ui.activities.createFactor.CreateFactorActivity;
import com.apptech.topstore.ui.activities.createFactor.CreateFactorViewModel;
import com.apptech.topstore.ui.activities.createFactor.contents.CreateFactorContent;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FactorsSoldProductsAdapter extends RecyclerView.Adapter<FactorsSoldProductsAdapter.myViewHolder>{
    List<SoldProductModel> items = new ArrayList<>();
    private Context mContext;
    private LayoutInflater inflater;
    private CreateFactorViewModel mViewModel;
    private CreateFactorContent mContent;



    public FactorsSoldProductsAdapter(Context context, List<SoldProductModel> arrayList, CreateFactorContent createFactorContent, CreateFactorViewModel viewModel){
        this.mContext = context;
        this.items = arrayList;
        inflater = LayoutInflater.from(context);
        this.mContent = createFactorContent;
        this.mViewModel = viewModel;


        Collections.sort(items, (o1, o2) -> String.valueOf(o1.getSoldID()).compareToIgnoreCase(String.valueOf(o2.getSoldID())));
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView = inflater.inflate(R.layout.item_factor_selling_product, parent, false);
        return new myViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {




        holder.mAmount.setText(ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getSoldAmount()), false));
        holder.mUnit.setText(Unit.getUnit(mContext, items.get(position).getSoldUnitID()));
        holder.mPrice.setText(Html.fromHtml("<html><body>" + ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getSoldAmount()*items.get(position).getSoldSellingPrice()), true) + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));



        mViewModel.getProducts(items.get(position).getSoldProductID()).observe(CreateFactorActivity.getInstance(), productModel -> holder.mProductName.setText(ConvertNumbers.convertToPersian(productModel.getProductName(),false)));

        holder.mPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (items.get(position).getSoldAmount() >= 0){
                    items.get(position).setSoldAmount(items.get(position).getSoldAmount() + 1);
                    mViewModel.update(items.get(position));
                    mContent.updateFactorPrice(items);
                    holder.mAmount.setText(ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getSoldAmount()), false));
                    holder.mPrice.setText(Html.fromHtml("<html><body>" + ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getSoldAmount()*items.get(position).getSoldSellingPrice()), true) + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));
                }
            }
        });
        holder.mMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (items.get(position).getSoldAmount() >= 0){
                    items.get(position).setSoldAmount(items.get(position).getSoldAmount() - 1);
                    mViewModel.update(items.get(position));
                    mContent.updateFactorPrice(items);
                    holder.mAmount.setText(ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getSoldAmount()), false));
                    holder.mPrice.setText(Html.fromHtml("<html><body>" + ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getSoldAmount()*items.get(position).getSoldSellingPrice()), true) + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));
                }
            }
        });

        holder.mCountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.mCountersLayout.getVisibility() == View.GONE){
                    holder.mCountersLayout.setVisibility(View.VISIBLE);
                } else {
                    holder.mCountersLayout.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public Context getContext() {
        return mContext;
    }

    class myViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.itemSoldProductName) TextView mProductName;
        @BindView(R.id.itemSoldAmount) TextView mAmount;
        @BindView(R.id.itemSoldUnit) TextView mUnit;
        @BindView(R.id.itemSoldCost) TextView mPrice;
        @BindView(R.id.itemSoldCount) LinearLayout mCountBtn;

        @BindView(R.id.itemSoldCounters) LinearLayout mCountersLayout;
        @BindView(R.id.itemSoldPlus) AppCompatImageView mPlus;
        @BindView(R.id.itemSoldMinus) AppCompatImageView mMinus;


        public myViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mProductName.setTypeface(Fonts.IranBold(mContext));
            mAmount.setTypeface(Fonts.IranNormal(mContext));
            mPrice.setTypeface(Fonts.IranNormal(mContext));
            mPrice.setTypeface(Fonts.IranBold(mContext));


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                itemView.setForeground(ContextCompat.getDrawable(mContext,R.drawable.ripple_accent_general));
                mCountBtn.setForeground(ContextCompat.getDrawable(mContext,R.drawable.ripple_accent));
                mPlus.setBackground(ContextCompat.getDrawable(mContext,R.drawable.ripple_round_accent_30dp));
                mMinus.setBackground(ContextCompat.getDrawable(mContext,R.drawable.ripple_round_accent_30dp));
            }




            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

        }
    }

    // TODO FIX THIS LATER
    public void deleteItem(int position) {
//        showUndoSnackBar(position, items.get(position));
        mViewModel.delete(items.get(position));
        items.remove(position);
        notifyItemRemoved(position);
    }
    private void showUndoSnackBar(int position, SoldProductModel soldProductModel) {
        View view = CreateFactorActivity.getInstance().findViewById(R.id.createFactorLayout);
        Snackbar snackbar = Snackbar.make(view, mContext.getString(R.string.product_removed), Snackbar.LENGTH_LONG);
        snackbar.setAction(mContext.getString(R.string.undo_remove), v -> undoDelete(position, soldProductModel));
        snackbar.show();
        snackbar.addCallback(new Snackbar.Callback(){
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                mViewModel.delete(soldProductModel);
            }
        });
    }
    private void undoDelete(int position, SoldProductModel soldProductModel) {
        items.add(position, soldProductModel);
        notifyItemInserted(position);
    }
}
