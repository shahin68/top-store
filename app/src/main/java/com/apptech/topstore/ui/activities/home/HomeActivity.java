package com.apptech.topstore.ui.activities.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.apptech.topstore.AppController;
import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.ui.activities.home.contents.HomeContent;
import com.apptech.topstore.ui.activities.home.navDrawer.NavDrawer;
import com.apptech.topstore.utils.StatusBarHeight;
import com.apptech.topstore.utils.Today;
import com.apptech.topstore.utils.preferences.MyPreferences;
import com.apptech.topstore.utils.preferences.MySharedPreferences;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }



    // --- set Content
    private HomeViewModel viewModel;
    HomeContent homeContent;
    public NavDrawer navDrawer;

    @Inject
    Today today;
    @Inject
    StatusBarHeight statusBarHeight;

    List<FactorModel> soldFactors;
    List<FactorModel> purchasedFactors;

    private static HomeActivity instance;
    public static HomeActivity getInstance() {
        return instance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        instance = this;
        AppController.get(this).getAppComponent().MyInject(this);
        setWindowDecor();
        homeContent = new HomeContent(this);
        navDrawer = new NavDrawer(this);
        homeContent.setTopMargin(statusBarHeight.get());
        navDrawer.setTopMargin(statusBarHeight.get());


        viewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        AppController.get(this).getAppComponent().MyInject(viewModel);


        homeContent.setTodayDate(today.weekDay(), today.date());

        soldFactors = new ArrayList<>();
        purchasedFactors = new ArrayList<>();
        viewModel.getFactors().observe(this, new Observer<List<FactorModel>>() {
            @Override
            public void onChanged(List<FactorModel> factorModels) {
                for (FactorModel factorModel : factorModels) {
                    /*if (factorModel.getFactorDateCreated()) { // if date is today

                    }*/
                    if (factorModel.getFactorType() == 2){ // sold
                        soldFactors.add(factorModel);
                    } else if (factorModel.getFactorType() == 1){ // bought
                        purchasedFactors.add(factorModel);
                    }
                }
                homeContent.setStatistics(soldFactors.size(), purchasedFactors.size());
            }
        });


    }
    private void setWindowDecor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(),R.color.fullTransparent));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            soldFactors = new ArrayList<>();
            purchasedFactors = new ArrayList<>();
            viewModel.getFactors().observe(this, new Observer<List<FactorModel>>() {
                @Override
                public void onChanged(List<FactorModel> factorModels) {
                    for (FactorModel factorModel : factorModels) {
                    /*if (factorModel.getFactorDateCreated()) { // if date is today

                    }*/
                        if (factorModel.getFactorType() == 2){ // sold
                            soldFactors.add(factorModel);
                        } else if (factorModel.getFactorType() == 1){ // bought
                            purchasedFactors.add(factorModel);
                        }
                    }
                    homeContent.setStatistics(soldFactors.size(), purchasedFactors.size());
                }
            });
        } catch (NullPointerException f){
            f.printStackTrace();
        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
