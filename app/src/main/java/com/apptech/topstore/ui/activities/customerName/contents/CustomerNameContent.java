package com.apptech.topstore.ui.activities.customerName.contents;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.apptech.topstore.BuildConfig;
import com.apptech.topstore.R;
import com.apptech.topstore.databaseLocal.Country;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;
import com.apptech.topstore.utils.SocialIntent;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerNameContent {
    // --- toolbar views
    @BindView(R.id.customerNameMessage) TextView mMessage;
    @BindView(R.id.customerNameBtn) Button mButton;

    // --- About Us Contents
    @BindView(R.id.customerNameInput) TextInputEditText mNameInput;


    // --- phone
    @BindView(R.id.customerNameCountryCode) LinearLayout mCountryCode; // Clickable
    @BindView(R.id.customerNameCountryCodeTxt) TextView mCountryCodeTxt;
    @BindView(R.id.customerNameCountryCodeFlag) AppCompatImageView mCountryFlag;
    @BindView(R.id.customerNamePhoneInput) TextInputEditText mPhoneInput;


    public CustomerNameContent(Activity activity){
        ButterKnife.bind(this, activity);
        mMessage.setTypeface(Fonts.IranBold(activity));
        mNameInput.setTypeface(Fonts.IranBold(activity));
        mPhoneInput.setTypeface(Fonts.IranBold(activity));
        mCountryCodeTxt.setTypeface(Fonts.IranBold(activity));
        mButton.setTypeface(Fonts.IranBold(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mButton.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mCountryCode.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
        }


        mCountryCode.setOnClickListener(v -> {
            showCountryMenu(activity, mCountryCode);
        });
        mButton.setOnClickListener(v -> {
            if (mNameInput.getText().toString().isEmpty() || mPhoneInput.getText().toString().isEmpty()){
                MyToast.error(activity.getString(R.string.fill_fields_first)).show();
            } else {
                finishActivity(activity,
                        mNameInput.getText().toString(),
                        mCountryCodeTxt.getText() + mPhoneInput.getText().toString()
                );
            }
        });
    }
    public void finishActivity(Activity context, String customerName, String phone) {
        MyLog.LogDebugging("CustomerName() ---> " + "NewProductContent() ---> " + "finishActivity()");
        Intent returnIntent = new Intent();
        returnIntent.putExtra("customer_name", customerName);
        returnIntent.putExtra("customer_phone", phone);
        context.setResult(1, returnIntent);
        context.finish();
    }

    private void showCountryMenu(Activity activity, View view){
        PopupMenu popup = new PopupMenu(activity, view);
        int count = Country.countryNames().length;
        for (int i = 0; i < count; i++){
            popup.getMenu().add(1,
                    i,
                    i,
                    ConvertNumbers.convertToPersian(Country.countryNames()[i].second, false))
                    .setIcon(Country.countryFlags()[i])
                    .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        popup.setOnMenuItemClickListener(
                item -> {
                    mCountryFlag.setImageDrawable(item.getIcon());
                    mCountryCodeTxt.setText(item.getTitle());
                    return true;
                });
        showPopupMenuIcons(popup);
        popup.show();
    }
    private void showPopupMenuIcons(PopupMenu popup){
        try {
            Field[] fields = popup.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popup);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
