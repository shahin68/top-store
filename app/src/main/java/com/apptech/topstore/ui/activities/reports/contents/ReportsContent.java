package com.apptech.topstore.ui.activities.reports.contents;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.aminography.primecalendar.PrimeCalendar;
import com.aminography.primecalendar.common.CalendarFactory;
import com.aminography.primecalendar.common.CalendarType;
import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.ui.activities.reports.ReportsViewModel;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.PersianDate;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.renderer.BarChartRenderer;
import com.github.mikephil.charting.renderer.DataRenderer;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportsContent {
    ReportsViewModel mViewModel;
    List<FactorModel> mFactorModels = new ArrayList<>();

    public List<FactorModel> getmFactorModels() {
        return mFactorModels;
    }

    public void setmFactorModels(Activity activity, List<FactorModel> mFactorModels) {
        this.mFactorModels = mFactorModels;
        setCharts(activity, getmFactorModels(), 2);
    }

    // --- toolbar views
    @BindView(R.id.reportsTitle) TextView mTitle;
    @BindView(R.id.reportsBack) LinearLayout mBack;
    @BindView(R.id.reportsTopDecor) AppCompatImageView mTopDecor;

    // --- Button Switches
    @BindView(R.id.reports_benefitsBtn) TextView mBenefitsBtn;
    @BindView(R.id.reports_purchasesBtn) TextView mPurchasesBtn;
    @BindView(R.id.reports_sellsBtn) TextView mSellsBtn;

    // --- report values
    @BindView(R.id.reports_stats_card) CardView mStatCard;
    @BindView(R.id.reports_inventory_value) TextView mInventoryValue;
    @BindView(R.id.reports_inventory_count) TextView mInventoryCount;

    // --- report Charts
    @BindView(R.id.reports_lineTxt) TextView mLineChartTxt;
    @BindView(R.id.reports_barTxt) TextView mBarChartTxt;
    @BindView(R.id.reports_line_chart) LineChart mLineChart;
    @BindView(R.id.reports_bar_chart) BarChart mBarChart;

    float[] dashes = new float[]{
            10f,
            5f
    };
    float[] formDashes = new float[]{
            24f,
            12f
    };

    private GradientDrawable leftBtnDrawable;
    private GradientDrawable middleDrawable;
    private GradientDrawable rightDrawable;

    long daysAgo7 = 604800000L; // or 604800
    long aMonthAgo = 2678400000L; // or 2678400

    public ReportsContent(Activity activity, ReportsViewModel reportsViewModel){
        this.mViewModel = reportsViewModel;
        ButterKnife.bind(this, activity);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mTopDecor);
        mBack.setOnClickListener(v -> activity.onBackPressed());
        mTitle.setTypeface(Fonts.IranBold(activity));
        mInventoryValue.setTypeface(Fonts.IranBold(activity));
        mInventoryCount.setTypeface(Fonts.IranBold(activity));
        mBenefitsBtn.setTypeface(Fonts.IranBold(activity));
        mPurchasesBtn.setTypeface(Fonts.IranBold(activity));
        mSellsBtn.setTypeface(Fonts.IranBold(activity));
        mLineChartTxt.setTypeface(Fonts.IranBold(activity));
        mBarChartTxt.setTypeface(Fonts.IranBold(activity));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mBack.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_white));
            mStatCard.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent));
            mBenefitsBtn.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_left));
            mPurchasesBtn.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_general));
            mSellsBtn.setForeground(ContextCompat.getDrawable(activity,R.drawable.ripple_accent_right));
        }


        leftBtnDrawable = (GradientDrawable) ContextCompat.getDrawable(activity, R.drawable.background_reports_left);
        middleDrawable = (GradientDrawable) ContextCompat.getDrawable(activity, R.drawable.background_reports_middle);
        rightDrawable = (GradientDrawable) ContextCompat.getDrawable(activity, R.drawable.background_reports_right);

        { // default selected type
            selected(activity, rightDrawable, mSellsBtn);
            unselected(activity, leftBtnDrawable, mBenefitsBtn);
            unselected(activity, leftBtnDrawable, mBenefitsBtn);
            setCharts(activity, getmFactorModels(), 2);
        }

        mBenefitsBtn.setOnClickListener(v -> {
            selected(activity, leftBtnDrawable, mBenefitsBtn);
            unselected(activity, middleDrawable, mPurchasesBtn);
            unselected(activity, rightDrawable, mSellsBtn);
        });
        mPurchasesBtn.setOnClickListener(v -> {
            setCharts(activity, getmFactorModels(), 1);
            selected(activity, middleDrawable, mPurchasesBtn);
            unselected(activity, leftBtnDrawable, mBenefitsBtn);
            unselected(activity, rightDrawable, mSellsBtn);
        });
        mSellsBtn.setOnClickListener(v -> {
            setCharts(activity, getmFactorModels(), 2);
            selected(activity, rightDrawable, mSellsBtn);
            unselected(activity, leftBtnDrawable, mBenefitsBtn);
            unselected(activity, middleDrawable, mPurchasesBtn);
        });
    }
    private void setCharts(Activity activity, List<FactorModel> factorModels, int type){
        if (factorModels.isEmpty()){
            List<ReportDataModel> lineData = new ArrayList<>();
            lineData.add(
                    new ReportDataModel(
                            0,
                            0,
                            "0"
                    )
            );
            setLineChart(activity, lineData);
            List<ReportDataModel> chartData = new ArrayList<>();
            chartData.add(
                    new ReportDataModel(
                            0,
                            0,
                            "0"
                    )
            );
            setBarChart(activity, chartData);
        } else {
            for (FactorModel factorModel : factorModels) {
                switch (type){
                    case 0:
                        // nothing for now
                        break;
                    case 1: // purchases
                        if (factorModel.getFactorType() == 1){ // purchased
                            if (factorModel.getFactorDateCreated() >= (System.currentTimeMillis() - daysAgo7)) {
                                List<ReportDataModel> reportDataModels = new ArrayList<>();
                                reportDataModels.add(
                                        new ReportDataModel(
                                                factorModel.getFactorDateCreated(),
                                                factorModel.getFactorCost(),
                                                PersianDate.weekDay(factorModel.getFactorDateCreated()).substring(0,1)
                                        )
                                );
                                setLineChart(activity, reportDataModels);
                            }
                            if (factorModel.getFactorDateCreated() >= (System.currentTimeMillis() - aMonthAgo)) {
                                List<ReportDataModel> reportDataModels = new ArrayList<>();
                                reportDataModels.add(
                                        new ReportDataModel(
                                                factorModel.getFactorDateCreated(),
                                                factorModel.getFactorCost(),
                                                PersianDate.weekDay(factorModel.getFactorDateCreated()).substring(0,1)
                                        )
                                );
                                setBarChart(activity, reportDataModels);
                            }
                        }
                        break;
                    case 2: // sales
                        if (factorModel.getFactorType() == 2){ // sold
                            if (factorModel.getFactorDateCreated() >= (System.currentTimeMillis() - daysAgo7)) {
                                List<ReportDataModel> reportDataModels = new ArrayList<>();
                                reportDataModels.add(
                                        new ReportDataModel(
                                                factorModel.getFactorDateCreated(),
                                                factorModel.getFactorCost(),
                                                PersianDate.weekDay(factorModel.getFactorDateCreated()).substring(0,1)
                                        )
                                );
                                setLineChart(activity, reportDataModels);
                            }
                            if (factorModel.getFactorDateCreated() >= (System.currentTimeMillis() - aMonthAgo)) {
                                List<ReportDataModel> reportDataModels = new ArrayList<>();
                                reportDataModels.add(
                                        new ReportDataModel(
                                                factorModel.getFactorDateCreated(),
                                                factorModel.getFactorCost(),
                                                PersianDate.weekDay(factorModel.getFactorDateCreated()).substring(0,1)
                                        )
                                );
                                setBarChart(activity, reportDataModels);
                            }
                        }
                        break;
                }
            }
        }
    }
    public void setStatistics(int inventoryValue, int inventoryCount) {
        mInventoryValue.setText(ConvertNumbers.convertToPersian(String.valueOf(inventoryValue), true));
        mInventoryCount.setText(ConvertNumbers.convertToPersian(String.valueOf(inventoryCount), false));
    }
    public void setLineChart(Context context, List<ReportDataModel> reportDataModels){
        List<Entry> entryList = new ArrayList<>();
        for (ReportDataModel reportDataModel : reportDataModels){
            Entry entry = new Entry(
                    reportDataModel.X_VALUE,
                    reportDataModel.Y_VALUE,
                    R.drawable.ic_android
            );
            entryList.add(entry);
        }

        List<ILineDataSet> lineDataSets = new ArrayList<>();
        for (ReportDataModel reportDataModel : reportDataModels){
            LineDataSet lineDataSet = new LineDataSet(entryList, reportDataModel.LABEL);
            lineDataSet.setDrawFilled(true);
            lineDataSet.setFillDrawable(ContextCompat.getDrawable(context, R.drawable.graph_gradient));
            lineDataSet.setLineWidth(4f);
            lineDataSet.setColor(ContextCompat.getColor(context , R.color.accent));
            lineDataSet.setCircleRadius(8f);
            lineDataSet.setCircleColor(ContextCompat.getColor(context , R.color.accent));
            lineDataSet.setDrawCircleHole(true);
            lineDataSet.setCircleHoleColor(ContextCompat.getColor(context , R.color.white));
            lineDataSet.setCircleHoleRadius(4f);
            lineDataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
            lineDataSet.setCubicIntensity(0.6f);
            lineDataSet.setValueTypeface(Fonts.IranBold(context));
            lineDataSet.setValueTextSize(13);
            lineDataSet.setDrawVerticalHighlightIndicator(true);
            lineDataSet.setDrawHorizontalHighlightIndicator(false);
            lineDataSet.setHighLightColor(ContextCompat.getColor(context , R.color.accent));
            lineDataSets.add(lineDataSet);
        }
        LineData lineData = new LineData(lineDataSets);
        mLineChart.setData(lineData);


        // --- axis lines
        mLineChart.getAxisRight().setDrawGridLines(false);
        mLineChart.getAxisRight().setDrawAxisLine(true);
        mLineChart.getAxisRight().setDrawZeroLine(true);
        mLineChart.getAxisRight().setZeroLineColor(ContextCompat.getColor(context , R.color.textLight));
        mLineChart.getAxisRight().setZeroLineWidth(2f);
        mLineChart.getAxisRight().setDrawTopYLabelEntry(false);
        mLineChart.getAxisRight().setDrawLimitLinesBehindData(false);
        mLineChart.getAxisRight().setDrawGridLinesBehindData(true);
        mLineChart.getAxisRight().setGridDashedLine(new DashPathEffect(dashes, 1));
        mLineChart.getAxisRight().setGridColor(ContextCompat.getColor(context , R.color.primaryDark));
        mLineChart.getAxisRight().setAxisLineColor(ContextCompat.getColor(context , R.color.textLight));
        mLineChart.getAxisRight().setTextColor(ContextCompat.getColor(context , R.color.textDark));
        mLineChart.getAxisRight().setDrawLabels(true);
        mLineChart.getAxisRight().setCenterAxisLabels(false);
        mLineChart.getAxisRight().setAxisLineWidth(2f);
        mLineChart.getAxisRight().setTypeface(Fonts.IranBold(context));
        mLineChart.getAxisRight().setTextSize(12);
        if (getYMin(reportDataModels) >= 0){
            mLineChart.getAxisRight().setAxisMinimum(0);
        } else {
            mLineChart.getAxisRight().setAxisMinimum(getYMin(reportDataModels));
        }
        mLineChart.getAxisRight().setAxisMaximum(getYMax(reportDataModels) + 15f);


        mLineChart.getXAxis().setDrawGridLines(true);
        mLineChart.getXAxis().setDrawAxisLine(false);
        mLineChart.getXAxis().setDrawLimitLinesBehindData(false);
        mLineChart.getXAxis().setDrawGridLinesBehindData(false);
        mLineChart.getXAxis().setGridDashedLine(new DashPathEffect(dashes, 1));
        mLineChart.getXAxis().setGridColor(ContextCompat.getColor(context , R.color.primaryDark));
        mLineChart.getXAxis().setAxisLineColor(ContextCompat.getColor(context , R.color.textLight));
        mLineChart.getXAxis().setTextColor(ContextCompat.getColor(context , R.color.textDark));
        mLineChart.getXAxis().setDrawLabels(true);
        mLineChart.getXAxis().setCenterAxisLabels(false);
        mLineChart.getXAxis().setTypeface(Fonts.IranBold(context));
        mLineChart.getXAxis().setTextSize(12);
        mLineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mLineChart.getXAxis().setAvoidFirstLastClipping(true);
        mLineChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
                Date date = new Date(Float.valueOf(value).longValue());
                primeCalendar.setTime(date);
                return primeCalendar.getDayOfMonth() + " " + primeCalendar.getWeekDayName().substring(0,1);
            }
        });


        mLineChart.getAxisLeft().setDrawAxisLine(false);
        mLineChart.getAxisLeft().setDrawZeroLine(false);
        mLineChart.getAxisLeft().setDrawLabels(false);
        mLineChart.getAxisLeft().setDrawTopYLabelEntry(false);
        mLineChart.getAxisLeft().setDrawGridLines(true);
        mLineChart.getAxisLeft().setDrawLimitLinesBehindData(false);
        mLineChart.getAxisLeft().setDrawGridLinesBehindData(true);
        mLineChart.getAxisLeft().setGridDashedLine(new DashPathEffect(dashes, 1));
        mLineChart.getAxisLeft().setGridColor(ContextCompat.getColor(context , R.color.primaryDark));
        mLineChart.getAxisLeft().setCenterAxisLabels(false);
        mLineChart.getAxisLeft().setTypeface(Fonts.IranBold(context));
        mLineChart.getAxisLeft().setTextSize(12);
        if (getYMin(reportDataModels) >= 0){
            mLineChart.getAxisLeft().setAxisMinimum(0);
        } else {
            mLineChart.getAxisLeft().setAxisMinimum(getYMin(reportDataModels));
        }
        mLineChart.getAxisLeft().setAxisMaximum(getYMax(reportDataModels) + 15f);


        mLineChart.getLegend().setEnabled(false);
        mLineChart.setClipValuesToContent(false);
        mLineChart.setDescription(null);
        mLineChart.setExtraBottomOffset(10);
        mLineChart.invalidate();

    }
    public void setBarChart(Context context, List<ReportDataModel> reportDataModels){
        List<BarEntry> entryList = new ArrayList<>();
        for (ReportDataModel reportDataModel : reportDataModels){
            BarEntry entry = new BarEntry(
                    Float.valueOf(reportDataModel.X_VALUE),
                    reportDataModel.Y_VALUE,
                    R.drawable.ic_android
            );
            entryList.add(entry);
        }

        List<IBarDataSet> barDataSets = new ArrayList<>();
        for (ReportDataModel reportDataModel : reportDataModels){
            BarDataSet barDataSet = new BarDataSet(entryList, reportDataModel.LABEL);
            barDataSet.setGradientColor(ContextCompat.getColor(context , R.color.primaryDark), ContextCompat.getColor(context , R.color.accentLight));
            barDataSet.setHighLightColor(ContextCompat.getColor(context , R.color.accent));
            barDataSet.setValueTypeface(Fonts.IranBold(context));
            barDataSet.setValueTextSize(13);
//            barDataSet.setFormLineWidth(24f);
            barDataSets.add(barDataSet);
        }
        BarData barData = new BarData(barDataSets);
        mBarChart.setData(barData);

        // --- axis lines
        mBarChart.getAxisRight().setDrawGridLines(false);
        mBarChart.getAxisRight().setDrawAxisLine(true);
        mBarChart.getAxisRight().setDrawZeroLine(true);
        mBarChart.getAxisRight().setZeroLineColor(ContextCompat.getColor(context , R.color.textLight));
        mBarChart.getAxisRight().setZeroLineWidth(2f);
        mBarChart.getAxisRight().setDrawTopYLabelEntry(false);
        mBarChart.getAxisRight().setDrawLimitLinesBehindData(true);
        mBarChart.getAxisRight().setDrawGridLinesBehindData(true);
        mBarChart.getAxisRight().setGridDashedLine(new DashPathEffect(dashes, 1));
        mBarChart.getAxisRight().setGridColor(ContextCompat.getColor(context , R.color.primaryDark));
        mBarChart.getAxisRight().setAxisLineColor(ContextCompat.getColor(context , R.color.textLight));
        mBarChart.getAxisRight().setTextColor(ContextCompat.getColor(context , R.color.textDark));
        mBarChart.getAxisRight().setDrawLabels(true);
        mBarChart.getAxisRight().setCenterAxisLabels(false);
        mBarChart.getAxisRight().setAxisLineWidth(2f);
        mBarChart.getAxisRight().setTypeface(Fonts.IranBold(context));
        mBarChart.getAxisRight().setTextSize(12);
        if (getYMin(reportDataModels) >= 0){
            mBarChart.getAxisRight().setAxisMinimum(0);
        } else {
            mBarChart.getAxisRight().setAxisMinimum(getYMin(reportDataModels));
        }
        mBarChart.getAxisRight().setAxisMaximum(getYMax(reportDataModels) + 15f);


        mBarChart.getXAxis().setDrawGridLines(true);
        mBarChart.getXAxis().setDrawAxisLine(false);
        mBarChart.getXAxis().setDrawLimitLinesBehindData(true);
        mBarChart.getXAxis().setDrawGridLinesBehindData(true);
        mBarChart.getXAxis().setGridDashedLine(new DashPathEffect(dashes, 1));
        mBarChart.getXAxis().setGridColor(ContextCompat.getColor(context , R.color.primaryDark));
        mBarChart.getXAxis().setAxisLineColor(ContextCompat.getColor(context , R.color.textLight));
        mBarChart.getXAxis().setTextColor(ContextCompat.getColor(context , R.color.textDark));
        mBarChart.getXAxis().setDrawLabels(true);
        mBarChart.getXAxis().setCenterAxisLabels(false);
        mBarChart.getXAxis().setTypeface(Fonts.IranBold(context));
        mBarChart.getXAxis().setTextSize(12);
        mBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mBarChart.getXAxis().setAvoidFirstLastClipping(true);
        mBarChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                PrimeCalendar primeCalendar = CalendarFactory.newInstance(CalendarType.PERSIAN);
                Date date = new Date(Float.valueOf(value).longValue());
                primeCalendar.setTime(date);
                return primeCalendar.getMonthName();
            }
        });


        mBarChart.getAxisLeft().setDrawAxisLine(false);
        mBarChart.getAxisLeft().setDrawZeroLine(false);
        mBarChart.getAxisLeft().setDrawLabels(false);
        mBarChart.getAxisLeft().setDrawTopYLabelEntry(false);
        mBarChart.getAxisLeft().setDrawGridLines(true);
        mBarChart.getAxisLeft().setDrawLimitLinesBehindData(true);
        mBarChart.getAxisLeft().setDrawGridLinesBehindData(true);
        mBarChart.getAxisLeft().setGridDashedLine(new DashPathEffect(dashes, 1));
        mBarChart.getAxisLeft().setGridColor(ContextCompat.getColor(context , R.color.primaryDark));
        mBarChart.getAxisLeft().setCenterAxisLabels(false);
        mBarChart.getAxisLeft().setTypeface(Fonts.IranBold(context));
        mBarChart.getAxisLeft().setTextSize(12);
        if (getYMin(reportDataModels) >= 0){
            mBarChart.getAxisLeft().setAxisMinimum(0);
        } else {
            mBarChart.getAxisLeft().setAxisMinimum(getYMin(reportDataModels));
        }
        mBarChart.getAxisLeft().setAxisMaximum(getYMax(reportDataModels) + 15f);


        mBarChart.getLegend().setEnabled(false);
        mBarChart.setDescription(null);
        mBarChart.setExtraBottomOffset(10);
        mBarChart.invalidate();
    }
    private float getYMin(List<ReportDataModel> reportDataModels){
        List<Float> yValues = new ArrayList<>();
        for (ReportDataModel reportDataModel : reportDataModels){
            yValues.add(reportDataModel.Y_VALUE);
        }
        return Collections.min(yValues);
    }
    private float getYMax(List<ReportDataModel> reportDataModels){
        List<Float> yValues = new ArrayList<>();
        for (ReportDataModel reportDataModel : reportDataModels){
            yValues.add(reportDataModel.Y_VALUE);
        }
        return Collections.max(yValues);
    }
    private float getXMin(List<ReportDataModel> reportDataModels){
        List<Float> xValues = new ArrayList<>();
        for (ReportDataModel reportDataModel : reportDataModels){
            xValues.add(reportDataModel.X_VALUE);
        }
        return Collections.min(xValues);
    }
    private float getXMax(List<ReportDataModel> reportDataModels){
        List<Float> xValues = new ArrayList<>();
        for (ReportDataModel reportDataModel : reportDataModels){
            xValues.add(reportDataModel.X_VALUE);
        }
        return Collections.max(xValues);
    }



    private void selected(Context context , GradientDrawable drawable, TextView textView){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (textView.getElevation() == 0){
                ValueAnimator animator = ValueAnimator.ofFloat(0, context.getResources().getDimensionPixelSize(R.dimen.dimen4));
                animator.addUpdateListener(animation -> textView.setElevation((Float) animation.getAnimatedValue()));
                animator.setDuration(300).start();
            }
        }
        drawable.setColor(ContextCompat.getColor(context, R.color.white));
        textView.setBackground(drawable);
        textView.setTextColor(ContextCompat.getColor(context, R.color.accent));
    }
    private void unselected(Context context , GradientDrawable drawable, TextView textView){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (textView.getElevation() != 0){
                ValueAnimator animator = ValueAnimator.ofFloat(context.getResources().getDimensionPixelSize(R.dimen.dimen4), 0);
                animator.addUpdateListener(animation -> textView.setElevation((Float) animation.getAnimatedValue()));
                animator.setDuration(300).start();
            }
        }
        drawable.setColor(ContextCompat.getColor(context, R.color.primary));
        textView.setBackground(drawable);
        textView.setTextColor(ContextCompat.getColor(context, R.color.textLightest));
    }


}
