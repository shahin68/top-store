package com.apptech.topstore.ui.activities.reports;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import com.apptech.topstore.AppController;
import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.ui.activities.reports.contents.ReportDataModel;
import com.apptech.topstore.ui.activities.reports.contents.ReportsContent;
import com.apptech.topstore.utils.PersianDate;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ReportsActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    private ReportsViewModel viewModel;
    ReportsContent reportsContent;
    int productsCount = 0;
    int productsCost = 0;
    int salesPrice = 0;
    int purchasesCost = 0;
    int rejectedCost = 0;
    int pureProfit = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        setWindowDecor();


        viewModel = ViewModelProviders.of(this).get(ReportsViewModel.class);
        AppController.get(this).getAppComponent().MyInject(viewModel);
        reportsContent = new ReportsContent(ReportsActivity.this, viewModel);
        viewModel.getFactors().observe(this, new Observer<List<FactorModel>>() {
            @Override
            public void onChanged(List<FactorModel> factorModels) {
                for (FactorModel factorModel : factorModels) {
                    if (factorModel.getFactorType() == 0){ // rejected
                        rejectedCost = rejectedCost + factorModel.getFactorCost();
                    } else if (factorModel.getFactorType() == 1) { // bought
                        purchasesCost = purchasesCost + factorModel.getFactorCost();
                    } else if (factorModel.getFactorType() == 2){ // sold
                        salesPrice = salesPrice + factorModel.getFactorCost();
                    }
                }
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        reportsContent.setmFactorModels(ReportsActivity.this, factorModels);
                    }
                }, 1000);
            }
        });
        viewModel.getProducts().observe(this, productModels -> {
            productsCount = productModels.size();
            for (ProductModel productModel : productModels) {
                productsCost = productsCost + productModel.getProductSellingPrice();
            }
            reportsContent.setStatistics(productsCost, productsCount);
        });







    }
    private void setWindowDecor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(),R.color.fullTransparent));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
