package com.apptech.topstore.ui.activities.newProduct;


import android.app.Activity;
import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.apptech.topstore.R;
import com.apptech.topstore.database.AppDatabase;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.ui.activities.newProduct.contents.NewProductContent;
import com.apptech.topstore.utils.MyLog;
import com.apptech.topstore.utils.MyToast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class NewProductViewModel extends ViewModel {


    @Inject
    AppDatabase appDatabase;
    Disposable disposable;

    LiveData<List<ProductModel>> getProducts() { return appDatabase.productsDao().getAllLive(); }


    public void insert(Context context, ProductModel productModel, NewProductContent newProductContent) {
        insertObservable(productModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Void>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        MyLog.LogDebugging("NewProductActivity() ---> " + "insertFactor() ---> " + "onSubscribe()");
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MyToast.error(context.getString(R.string.store_product_failed)).show();
                        MyLog.LogDebugging("NewProductActivity() ---> " + "insertFactor() ---> " + "onError()");
                    }

                    @Override
                    public void onComplete() {
                        newProductContent.clearEntries(context);
                        MyLog.LogDebugging("NewProductActivity() ---> " + "insertFactor() ---> " + "onComplete()");
                    }
                });
    }
    private Observable<Void> insertObservable(ProductModel productModel) {
        return new Observable<Void>() {
            @Override
            protected void subscribeActual(Observer<? super Void> observer) {
                appDatabase.productsDao().insertOne(productModel);
                observer.onSubscribe(new Disposable() {
                    @Override
                    public void dispose() {

                    }

                    @Override
                    public boolean isDisposed() {
                        return false;
                    }
                });
                observer.onComplete();
            }
        };
    }
    public void insertForType(Activity context, ProductModel productModel, NewProductContent newProductContent) {
        insertForTypeObservable(productModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Void>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        MyLog.LogDebugging("NewProductActivity() ---> " + "insertForType() ---> " + "onSubscribe()");
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MyToast.error(context.getString(R.string.store_product_failed)).show();
                        MyLog.LogDebugging("NewProductActivity() ---> " + "insertForType() ---> " + "onError()");
                    }

                    @Override
                    public void onComplete() {
                        List<ProductModel> productModelList = new ArrayList<>();
                        productModelList.add(productModel);
                        newProductContent.finishActivity(context, productModelList);
                        MyLog.LogDebugging("NewProductActivity() ---> " + "insertForType() ---> " + "onComplete()");
                    }
                });
    }
    private Observable<Void> insertForTypeObservable(ProductModel productModel) {
        return new Observable<Void>() {
            @Override
            protected void subscribeActual(Observer<? super Void> observer) {
                appDatabase.productsDao().insertOne(productModel);
                observer.onSubscribe(new Disposable() {
                    @Override
                    public void dispose() {

                    }

                    @Override
                    public boolean isDisposed() {
                        return false;
                    }
                });
                observer.onComplete();
            }
        };
    }



}
