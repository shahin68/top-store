package com.apptech.topstore.ui.activities.factorDetails.FactorDetailsData;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.R;
import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.database.factors.soldProducts.SoldProductModel;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.databaseLocal.Unit;
import com.apptech.topstore.ui.activities.factorDetails.FactorDetailsActivity;
import com.apptech.topstore.ui.activities.factorDetails.FactorDetailsViewModel;
import com.apptech.topstore.ui.activities.factors.FactorsActivity;
import com.apptech.topstore.ui.activities.factors.FactorsViewModel;
import com.apptech.topstore.utils.ConvertNumbers;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.PersianDate;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FactorDetailsAdapter extends RecyclerView.Adapter<FactorDetailsAdapter.myViewHolder>{
    List<SoldProductModel> items = new ArrayList<>();
    private Context mContext;
    private LayoutInflater inflater;
    private FactorDetailsViewModel mViewModel;
    FactorModel mFactorModel;

    public Context getContext() {
        return mContext;
    }

    public FactorDetailsAdapter(Context context, List<SoldProductModel> arrayList, FactorModel factorModel, FactorDetailsViewModel factorsViewModel){
        this.mContext = context;
        this.items = arrayList;
        this.inflater = LayoutInflater.from(context);
        this.mViewModel = factorsViewModel;
        this.mFactorModel = factorModel;

        Collections.sort(items, (o1, o2) -> String.valueOf(o1.getSoldID()).compareToIgnoreCase(String.valueOf(o2.getSoldID())));
    }
    private String latinNumOf(String num) {
        return ConvertNumbers.convertToLatin(num);
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView = inflater.inflate(R.layout.item_storage, parent, false);
        return new myViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {



        holder.mAmount.setText(ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getSoldAmount()), false) + " " + Unit.getUnit(mContext, items.get(position).getSoldUnitID()));
        holder.mOneUnitPrice.setText(Html.fromHtml("<html><body>" + ConvertNumbers.convertToPersian(String.valueOf(items.get(position).getSoldSellingPrice()), true) + "<small>" + " " + mContext.getString(R.string.toman) +"</small></body><html>"));


        mViewModel.getProducts(items.get(position).getSoldProductID()).observe(FactorDetailsActivity.getInstance(), productModel -> holder.mTitle.setText(ConvertNumbers.convertToPersian(productModel.getProductName(),false)));



    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class myViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.itemStorageProduct) TextView mTitle;
        @BindView(R.id.itemStorageAmount) TextView mAmount;
        @BindView(R.id.itemStorageUnitPrice) TextView mOneUnitPrice;



        public myViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mTitle.setTypeface(Fonts.IranBold(mContext));
            mAmount.setTypeface(Fonts.IranBold(mContext));
            mOneUnitPrice.setTypeface(Fonts.IranBold(mContext));


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                itemView.setForeground(ContextCompat.getDrawable(mContext,R.drawable.ripple_accent_general));
            }




            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

        }
    }

    // TODO FIX THIS LATER
    public void deleteItem(int position) {
//        showUndoSnackBar(position, items.get(position));
        mViewModel.delete(items.get(position));
        items.remove(position);
        notifyItemRemoved(position);
        updateFactor();
    }
    private void updateFactor(){
        int factorCost = 0;
        for (SoldProductModel model : items){
            factorCost = factorCost + (model.getSoldAmount()*model.getSoldSellingPrice());
        }
        mFactorModel.setFactorCost(factorCost);
        mViewModel.updateFactor(mFactorModel);
    }
    private void showUndoSnackBar(int position, SoldProductModel soldProduct) {
        View view = FactorsActivity.getInstance().findViewById(R.id.factors_layout);
        Snackbar snackbar = Snackbar.make(view, mContext.getString(R.string.product_removed), Snackbar.LENGTH_LONG);
        snackbar.setAction(mContext.getString(R.string.undo_remove), v -> undoDelete(position, soldProduct));
        snackbar.show();
        snackbar.addCallback(new Snackbar.Callback(){
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                mViewModel.delete(soldProduct);
            }
        });
    }
    private void undoDelete(int position, SoldProductModel soldProduct) {
        items.add(position, soldProduct);
        notifyItemInserted(position);
    }
}
