package com.apptech.topstore.ui.activities.home.navDrawer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apptech.topstore.BuildConfig;
import com.apptech.topstore.R;
import com.apptech.topstore.utils.Fonts;
import com.apptech.topstore.utils.MyLog;
import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NavDrawer {


    // --- Nav Drawer Views
    @BindView(R.id.menu_button) AppCompatImageView mMenuButton;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.home_nav_view) NavigationView mNavigationView;
    @BindView(R.id.drawer_list) RecyclerView mDrawerList;
    @BindView(R.id.home_nav_logout) LinearLayout mLogOut;
    @BindView(R.id.home_nav_footer_txt) TextView mLogOutTxt;
    @BindView(R.id.navStoreName) TextView mStoreName;
    @BindView(R.id.navUserName) TextView mUserName;
    @BindView(R.id.navImage) AppCompatImageView mNaveImage;
    @BindView(R.id.navHeaderUser) LinearLayout mNavUser;
    @BindView(R.id.navHeaderTop) LinearLayout mNavTop;
    @BindView(R.id.navBack) AppCompatImageView mNavBack;
    @BindView(R.id.home_anv_appVersion) TextView mNavAppVersion;

    public NavDrawer(Activity activity){
        MyLog.LogDebugging("HomeActivity() ---> " + "NavDrawer() ---> " + "init");
        ButterKnife.bind(this, activity);
        setNavDrawerBehaviour(activity);
        Glide.with(activity).load(R.drawable.decor_toolbar).into(mNaveImage);
        mNavUser.bringToFront();
        mNavTop.bringToFront();
        mNavBack.setOnClickListener(v -> closeDrawer());
        mLogOutTxt.setTypeface(Fonts.IranBold(activity));
        mStoreName.setTypeface(Fonts.IranBold(activity));
        setRipples(activity);
        mNavAppVersion.setText(activity.getString(R.string.version) + " " + BuildConfig.VERSION_NAME);
    }
    public void setTopMargin(int statusHeight){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mNavTop.getLayoutParams();
        layoutParams.topMargin = statusHeight;
        mNavTop.setLayoutParams(layoutParams);
    }
    private void setNavDrawerBehaviour(Context context){
        MyLog.LogDebugging("HomeActivity() ---> " + "NavDrawer() ---> " + "setNavDrawerBehaviour()");
        if (!DrawerItem.folderItems.isEmpty()){
            DrawerItem.folderItems.clear();
        }
        for (int i = 0; i < NavDrawerData.data().length; i++){
            DrawerItem item = new DrawerItem();
            item.nav_title = context.getString(NavDrawerData.data()[i].first);
            item.nav_img = NavDrawerData.data()[i].second;
            DrawerItem.folderItems.add(item);
        }

        DrawerAdapter mNavAdapter = new DrawerAdapter(context, DrawerItem.folderItems);
        mDrawerList.setAdapter(mNavAdapter);
        mDrawerList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        mNavAdapter.notifyDataSetChanged();
        mDrawerList.setNestedScrollingEnabled(false);
        mMenuButton.setOnClickListener(v -> openDrawer());

    }
    private void setRipples(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            mLogOut.setForeground(ContextCompat.getDrawable(context,R.drawable.ripple_accent_general));
            mNavUser.setForeground(ContextCompat.getDrawable(context,R.drawable.ripple_white_general));
        }
    }

    @SuppressLint("RtlHardcoded")
    void openDrawer() {
        mDrawerLayout.openDrawer(Gravity.RIGHT);
    }
    @SuppressLint("RtlHardcoded")
    void closeDrawer() {
        mDrawerLayout.closeDrawer(Gravity.RIGHT);
    }


}
