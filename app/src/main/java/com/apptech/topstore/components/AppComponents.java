package com.apptech.topstore.components;



import com.apptech.topstore.modules.AppModule;
import com.apptech.topstore.modules.DatabaseModule;
import com.apptech.topstore.modules.StatusBarModule;
import com.apptech.topstore.modules.TodayModule;
import com.apptech.topstore.ui.activities.createFactor.CreateFactorViewModel;
import com.apptech.topstore.ui.activities.factorDetails.FactorDetailsViewModel;
import com.apptech.topstore.ui.activities.factors.FactorsViewModel;
import com.apptech.topstore.ui.activities.home.HomeActivity;
import com.apptech.topstore.ui.activities.home.HomeViewModel;
import com.apptech.topstore.ui.activities.newProduct.NewProductActivity;
import com.apptech.topstore.ui.activities.newProduct.NewProductViewModel;
import com.apptech.topstore.ui.activities.reports.ReportsViewModel;
import com.apptech.topstore.ui.activities.storage.StorageViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, DatabaseModule.class, TodayModule.class, StatusBarModule.class})
public interface AppComponents {
    void MyInject(HomeActivity homeActivity);
    void MyInject(NewProductViewModel newProductViewModel);
    void MyInject(StorageViewModel storageViewModel);
    void MyInject(FactorsViewModel factorsViewModel);
    void MyInject(CreateFactorViewModel createFactorViewModel);
    void MyInject(ReportsViewModel reportsViewModel);
    void MyInject(HomeViewModel homeViewModel);
    void MyInject(FactorDetailsViewModel factorDetailsViewModel);
}
