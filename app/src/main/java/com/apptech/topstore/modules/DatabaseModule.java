package com.apptech.topstore.modules;

import androidx.annotation.NonNull;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.apptech.topstore.AppController;
import com.apptech.topstore.database.AppDatabase;
import com.apptech.topstore.database.products.ProductsDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    @Singleton
    @Provides
    AppDatabase appDatabase(){
        return Room.databaseBuilder(AppController.getContext(), AppDatabase.class, "database-top-store").build();
    }

}
