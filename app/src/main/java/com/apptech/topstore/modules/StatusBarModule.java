package com.apptech.topstore.modules;

import com.apptech.topstore.utils.StatusBarHeight;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StatusBarModule {
    @Singleton
    @Provides
    StatusBarHeight getStatusBarHeight(){
        return new StatusBarHeight();
    }
}
