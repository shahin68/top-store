package com.apptech.topstore.modules;

import com.apptech.topstore.utils.Today;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class TodayModule {
    @Singleton
    @Provides
    Today getWeekDay(){
        return new Today();
    }

}
