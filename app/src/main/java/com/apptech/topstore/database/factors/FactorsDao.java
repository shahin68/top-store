package com.apptech.topstore.database.factors;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.apptech.topstore.database.factors.soldProducts.SoldProductModel;

import java.util.List;

@Dao
public interface FactorsDao {
    @Query("SELECT * FROM factors")
    List<FactorModel> getAllFactors();

    @Query("SELECT * FROM factors WHERE factorID IN (:factorIDs)")
    List<FactorModel> loadAllFactorsByIds(int[] factorIDs);

    @Query("SELECT * FROM factors WHERE factorReferenceCode LIKE :referenceCode LIMIT 1")
    FactorModel findFactorByReferenceCode(String referenceCode);

    @Query("SELECT * FROM factors WHERE factorID LIKE :factorID LIMIT 1")
    LiveData<FactorModel> findFactorByID(int factorID);

    @Query("SELECT * FROM factors WHERE factorUserID LIKE :userID LIMIT 1")
    FactorModel findFactorByUserID(int userID);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFactorAll(List<FactorModel> factorModels);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertOneFactor(FactorModel factorModel);

    @Delete
    void deleteFactor(FactorModel factorModel);

    @Query("SELECT * from factors ORDER BY factorID ASC")
    LiveData<List<FactorModel>> getAllFactorsLive();


    @Update
    void updateOne(FactorModel factorModel);

}
