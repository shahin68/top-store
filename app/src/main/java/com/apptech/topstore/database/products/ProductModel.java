package com.apptech.topstore.database.products;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "products")
public class ProductModel {
    @PrimaryKey
    private int productID;
    @ColumnInfo(name = "productBarcode")
    private int productBarcode;
    @ColumnInfo(name = "productName")
    private String productName;
    @ColumnInfo(name = "productDescription")
    private String productDescription;
    @ColumnInfo(name = "productBasePrice")
    private int productBasePrice;
    @ColumnInfo(name = "productSellingPrice")
    private int productSellingPrice;
    @ColumnInfo(name = "productUnitID")
    private int productUnitId;
    @ColumnInfo(name = "productAmount")
    private int productAmount;
    @ColumnInfo(name = "productGroupIDs")
    private int productGroupIDs;

    public ProductModel(int productID, int productBarcode, String productName, String productDescription, int productBasePrice, int productSellingPrice, int productUnitId, int productAmount, int productGroupIDs) {
        this.productID = productID;
        this.productBarcode = productBarcode;
        this.productName = productName;
        this.productDescription = productDescription;
        this.productBasePrice = productBasePrice;
        this.productSellingPrice = productSellingPrice;
        this.productUnitId = productUnitId;
        this.productAmount = productAmount;
        this.productGroupIDs = productGroupIDs;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getProductBarcode() {
        return productBarcode;
    }

    public void setProductBarcode(int productBarcode) {
        this.productBarcode = productBarcode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getProductBasePrice() {
        return productBasePrice;
    }

    public void setProductBasePrice(int productBasePrice) {
        this.productBasePrice = productBasePrice;
    }

    public int getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(int productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    public int getProductUnitId() {
        return productUnitId;
    }

    public void setProductUnitId(int productUnitId) {
        this.productUnitId = productUnitId;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }

    public int getProductGroupIDs() {
        return productGroupIDs;
    }

    public void setProductGroupIDs(int productGroupIDs) {
        this.productGroupIDs = productGroupIDs;
    }
}
