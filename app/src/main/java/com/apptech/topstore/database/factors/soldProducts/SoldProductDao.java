package com.apptech.topstore.database.factors.soldProducts;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SoldProductDao {
    @Query("SELECT * FROM soldProducts")
    List<SoldProductModel> getAll();

    @Query("SELECT * FROM soldProducts WHERE soldID IN (:productIds)")
    List<SoldProductModel> loadAllByIds(int[] productIds);

    @Query("SELECT * FROM soldProducts WHERE factorID IN (:factorID)")
    LiveData<List<SoldProductModel>> findSoldProductsByFactorID(int factorID);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<SoldProductModel> productModels);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertOne(SoldProductModel productModels);

    @Delete
    void delete(SoldProductModel productModel);

    @Query("SELECT * from soldProducts ORDER BY soldID ASC")
    LiveData<List<SoldProductModel>> getAllLive();

    @Update
    void updateOne(SoldProductModel soldProductModel);
}
