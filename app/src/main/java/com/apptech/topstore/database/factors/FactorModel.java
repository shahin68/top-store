package com.apptech.topstore.database.factors;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "factors")
public class FactorModel {
    @PrimaryKey
    public int factorID;
    @ColumnInfo(name = "factorReferenceCode")
    private String factorReferenceCode;
    @ColumnInfo(name = "factorDateCreated")
    private long factorDateCreated;
    @ColumnInfo(name = "factorCustomerName")
    private String factorCustomerName;
    @ColumnInfo(name = "factorCustomerContact")
    private String factorCustomerContact;
    @ColumnInfo(name = "factorStatus")
    private int factorStatus; // 0 = drafted, 1 = completed, 2 = deleted
    @ColumnInfo(name = "factorUserID")
    private int factorUserID;
    @ColumnInfo(name = "factorType")
    private int factorType; // 0 = rejected, 1 = bought, 2 = sold
    @ColumnInfo(name = "factorProductCount")
    private int factorProductCount;
    @ColumnInfo(name = "factorCost")
    private int factorCost;

    public FactorModel(int factorID, String factorReferenceCode, long factorDateCreated, String factorCustomerName, String factorCustomerContact, int factorStatus, int factorUserID, int factorType, int factorProductCount, int factorCost) {
        this.factorID = factorID;
        this.factorReferenceCode = factorReferenceCode;
        this.factorDateCreated = factorDateCreated;
        this.factorCustomerName = factorCustomerName;
        this.factorCustomerContact = factorCustomerContact;
        this.factorStatus = factorStatus;
        this.factorUserID = factorUserID;
        this.factorType = factorType;
        this.factorProductCount = factorProductCount;
        this.factorCost = factorCost;
    }

    public int getFactorID() {
        return factorID;
    }

    public void setFactorID(int factorID) {
        this.factorID = factorID;
    }

    public String getFactorReferenceCode() {
        return factorReferenceCode;
    }

    public void setFactorReferenceCode(String factorReferenceCode) {
        this.factorReferenceCode = factorReferenceCode;
    }

    public long getFactorDateCreated() {
        return factorDateCreated;
    }

    public void setFactorDateCreated(long factorDateCreated) {
        this.factorDateCreated = factorDateCreated;
    }

    public String getFactorCustomerName() {
        return factorCustomerName;
    }

    public void setFactorCustomerName(String factorCustomerName) {
        this.factorCustomerName = factorCustomerName;
    }

    public String getFactorCustomerContact() {
        return factorCustomerContact;
    }

    public void setFactorCustomerContact(String factorCustomerContact) {
        this.factorCustomerContact = factorCustomerContact;
    }

    public int getFactorStatus() {
        return factorStatus;
    }

    public void setFactorStatus(int factorStatus) {
        this.factorStatus = factorStatus;
    }

    public int getFactorUserID() {
        return factorUserID;
    }

    public void setFactorUserID(int factorUserID) {
        this.factorUserID = factorUserID;
    }

    public int getFactorType() {
        return factorType;
    }

    public void setFactorType(int factorType) {
        this.factorType = factorType;
    }

    public int getFactorProductCount() {
        return factorProductCount;
    }

    public void setFactorProductCount(int factorProductCount) {
        this.factorProductCount = factorProductCount;
    }

    public int getFactorCost() {
        return factorCost;
    }

    public void setFactorCost(int factorCost) {
        this.factorCost = factorCost;
    }
}
