package com.apptech.topstore.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.apptech.topstore.database.factors.FactorModel;
import com.apptech.topstore.database.factors.FactorsDao;
import com.apptech.topstore.database.factors.soldProducts.SoldProductDao;
import com.apptech.topstore.database.factors.soldProducts.SoldProductModel;
import com.apptech.topstore.database.products.ProductModel;
import com.apptech.topstore.database.products.ProductsDao;

@Database(entities = {ProductModel.class, FactorModel.class, SoldProductModel.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ProductsDao productsDao();
    public abstract FactorsDao factorsDao();
    public abstract SoldProductDao soldProductDao();


    /*private static volatile ProductsDao INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static ProductsDao getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ProductsDao.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ProductsDao.class, "word_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }*/
}
