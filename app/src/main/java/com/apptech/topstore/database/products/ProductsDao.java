package com.apptech.topstore.database.products;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Observable;


@Dao
public interface  ProductsDao {
    @Query("SELECT * FROM products")
    List<ProductModel> getAll();

    @Query("SELECT * FROM products WHERE productID IN (:productIds)")
    List<ProductModel> loadAllByIds(int[] productIds);

    @Query("SELECT * FROM products WHERE productID LIKE :productID LIMIT 1")
    LiveData<ProductModel> findProductByID(int productID);

    @Query("SELECT * FROM products WHERE productName LIKE :productName LIMIT 1")
    ProductModel findByName(String productName);

    @Query("SELECT * FROM products WHERE productBarcode LIKE :productBarcode LIMIT 1")
    ProductModel findByBarcode(int productBarcode);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(List<ProductModel> productModels);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertOne(ProductModel productModels);

    @Delete
    void delete(ProductModel productModel);

    @Query("SELECT * from products ORDER BY productID ASC")
    LiveData<List<ProductModel>> getAllLive();
}
