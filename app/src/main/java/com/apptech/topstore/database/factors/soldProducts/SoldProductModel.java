package com.apptech.topstore.database.factors.soldProducts;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "soldProducts")
public class SoldProductModel {
    @PrimaryKey
    @NonNull
    public String soldID;
    @ColumnInfo(name = "factorID")
    public int factorID;
    @ColumnInfo(name = "soldProductID")
    private int soldProductID;
    @ColumnInfo(name = "soldUnitID")
    private int soldUnitID;
    @ColumnInfo(name = "soldAmount")
    private int soldAmount;
    @ColumnInfo(name = "soldBasePrice")
    private int soldBasePrice;
    @ColumnInfo(name = "soldSellingPrice")
    private int soldSellingPrice;


    public SoldProductModel(String soldID, int factorID, int soldProductID, int soldUnitID, int soldAmount, int soldBasePrice, int soldSellingPrice) {
        this.soldID = soldID;
        this.factorID = factorID;
        this.soldProductID = soldProductID;
        this.soldUnitID = soldUnitID;
        this.soldAmount = soldAmount;
        this.soldBasePrice = soldBasePrice;
        this.soldSellingPrice = soldSellingPrice;
    }

    public String getSoldID() {
        return soldID;
    }

    public void setSoldID(String soldID) {
        this.soldID = soldID;
    }

    public int getFactorID() {
        return factorID;
    }

    public void setFactorID(int factorID) {
        this.factorID = factorID;
    }

    public int getSoldProductID() {
        return soldProductID;
    }

    public void setSoldProductID(int soldProductID) {
        this.soldProductID = soldProductID;
    }

    public int getSoldUnitID() {
        return soldUnitID;
    }

    public void setSoldUnitID(int soldUnitID) {
        this.soldUnitID = soldUnitID;
    }

    public int getSoldAmount() {
        return soldAmount;
    }

    public void setSoldAmount(int soldAmount) {
        this.soldAmount = soldAmount;
    }

    public int getSoldBasePrice() {
        return soldBasePrice;
    }

    public void setSoldBasePrice(int soldBasePrice) {
        this.soldBasePrice = soldBasePrice;
    }

    public int getSoldSellingPrice() {
        return soldSellingPrice;
    }

    public void setSoldSellingPrice(int soldSellingPrice) {
        this.soldSellingPrice = soldSellingPrice;
    }
}
