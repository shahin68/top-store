package com.apptech.topstore;

import com.apptech.topstore.database.factors.FactorModel;

import java.util.List;

public class GenerateFactorID {
    public static int newID_Descending(List<FactorModel> mFactorModels){
        try {
            if (mFactorModels.isEmpty()){
                return 1;
            } else {
                try {
                    return mFactorModels.get(0).getFactorID() + 1;
                } catch (NullPointerException f){
                    return 1;
                }
            }
        } catch (NullPointerException f){
            return 1;
        }
    }
    public static int newID_Ascending(List<FactorModel> mFactorModels){
        try {
            if (mFactorModels.isEmpty()){
                return 1;
            } else {
                try {
                    return mFactorModels.get(mFactorModels.size()-1).getFactorID() + 1;
                } catch (NullPointerException f){
                    return 1;
                }
            }
        } catch (NullPointerException f){
            return 1;
        }
    }
    public static int nowID_Descending(List<FactorModel> mFactorModels){
        if (mFactorModels.isEmpty()){
            return 1;
        } else {
            return mFactorModels.get(0).getFactorID();
        }
    }
    public static int nowID_Ascending(List<FactorModel> mFactorModels){
        if (mFactorModels.isEmpty()){
            return 1;
        } else {
            return mFactorModels.get(mFactorModels.size()-1).getFactorID();
        }
    }


}
