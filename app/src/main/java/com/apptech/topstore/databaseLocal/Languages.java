package com.apptech.topstore.databaseLocal;

import android.content.res.Resources;

public class Languages {
    public static String[] get(){
        return Resources.getSystem().getAssets().getLocales();
    }
}
